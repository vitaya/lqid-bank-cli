module.exports =
  `import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import { __%COMPONENT_NAME%__ } from '../'

describe('__%COMPONENT_NAME%__ Component', () => {
  describe('renderer', () => {
    it('should be render correctly', () => {
      const wrapper = new ShallowRenderer().render(<__%COMPONENT_NAME%__ />)
      expect(wrapper).toMatchSnapshot()
    })
  })
})`
