module.exports =
  `import React from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { __%COMPONENT_NAME%__Style as styles } from './stylesheets'

function __%COMPONENT_NAME%__(props) {
  return (
    <View>
      <Text>__%COMPONENT_NAME%__</Text>
    </View>
  )
} 

__%COMPONENT_NAME%__.propTypes = {}
__%COMPONENT_NAME%__.defaultProps = {}

export default __%COMPONENT_NAME%__
`