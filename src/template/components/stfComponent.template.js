module.exports =
  `import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { __%COMPONENT_NAME%__Style as styles } from './stylesheets'

class __%COMPONENT_NAME%__ extends Component {

  constructor(props) {
    super(props)
    this.state = { 
      /** Add State Here **/
    }
  }

  static defaultProps = {
    // Add Default Props here
  }

  static propTypes = {
    // Add Prop-Types here
  }

  componentDidMount() {
    // TODO: Add Action Here
  }

  render() {
    return (
      <View>
        <Text>__%COMPONENT_NAME%__</Text>
      </View>
    )
  }
} 

export default __%COMPONENT_NAME%__
`