module.exports = `
import React from 'react'
import PropTypes from 'prop-types'
import { __%CONTAINER_NAME%__ } from '../containers'

const __%SCENE_NAME%__ = () => <__%CONTAINER_NAME%__ />

__%SCENE_NAME%__.propTypes = {}

export default __%SCENE_NAME%__
`