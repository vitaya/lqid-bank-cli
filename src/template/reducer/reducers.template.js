module.exports = `
import { LOG_OUT } from '@Constants'
import { initialState } from './initialState'
import { ACTION_TYPE_NAME } from './actionTypes'

const REDUCER_NAMEReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPE_NAME.REQUEST: {
      return {
        ...state,
        isFetching: true,
        code: initialState.code,
      }
    }

    case ACTION_TYPE_NAME.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        error: initialState.error,
        code: action.code,
        data: action.data
      }
    }

    case ACTION_TYPE_NAME.FAILURE: {
      return {
        ...state,
        isFetching: false,
        code: action.code,
        error: action.error,
        data: initialState.data
      }
    }

    case LOG_OUT:
      return initialState
    default:
      return state
  }
}

export default REDUCER_NAMEReducer
`