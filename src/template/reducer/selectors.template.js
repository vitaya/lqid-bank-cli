module.exports = `
import _get from 'lodash/get'
import { project } from '@Configs'

export const selectREDUCER_NAME = (state, name) => _get(state[project.name], \`REDUCER_NAME.\${name}\`, state[project.name].REDUCER_NAME)
`