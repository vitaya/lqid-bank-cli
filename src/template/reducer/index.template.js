module.exports = `
import reducers from './reducers'
import { initialState } from './initialState'
import { ACTION_CREATOR_NAME } from './actionCreators'
import { selectREDUCER_NAME } from './selectors'

export {
  reducers,
  initialState,
  ACTION_CREATOR_NAME,
  selectREDUCER_NAME,
}
`