module.exports = `
import { actionTypeCreator } from '@Utils'

export const ACTION_TYPE_NAME = actionTypeCreator('ACTION_TYPE_NAME')
`