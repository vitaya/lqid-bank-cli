module.exports = `
import _get from 'lodash/get'
import { handleErrorResponse } from '@Utils'
import { initialState } from './initialState'
import { SERVICE_NAME as SERVICE_NAMEService } from '../../services'
import { ACTION_TYPE_NAME } from './actionTypes'

const ACTION_CREATOR_NAMERequest = () => ({
  type: ACTION_TYPE_NAME.REQUEST
})

const ACTION_CREATOR_NAMESuccess = (response) => {
  const data = _get(response, 'data.data', initialState.data)
  const code = _get(response, 'data.code', initialState.code)
  return {
    type: ACTION_TYPE_NAME.SUCCESS,
    data,
    code,
  }
}

const ACTION_CREATOR_NAMEFailure = (errors) => {
  const { error, code } = handleErrorResponse(errors)
  return {
    type: ACTION_TYPE_NAME.FAILURE,
    error,
    code,
  }
}

export const ACTION_CREATOR_NAME = (accountInfo) => (dispatch) => {
  dispatch(ACTION_CREATOR_NAMERequest())
  return SERVICE_NAMEService(accountInfo)
    .then(response => dispatch(ACTION_CREATOR_NAMESuccess(response)))
    .catch(errors => dispatch(ACTION_CREATOR_NAMEFailure(errors)))
}
`