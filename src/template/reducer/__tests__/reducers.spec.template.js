module.exports = `
import { handleErrorResponse } from '@Utils'
import { LOG_OUT } from '@Constants'
import reducers from '../reducers'
import { initialState } from '../initialState'
import { ACTION_TYPE_NAME } from '../actionTypes'
import { mock as mockResponse } from '../../../services/SERVICE_NAME'

describe('REDUCER_NAME reducer', () => {
  it('should return initial state', () => {
    expect(reducers(undefined, {})).toEqual(initialState)
  })

  it('should not handle when action type is not match handle case', () => {
    const action = { type: 'SOME_ACTION_TYPE' }
    const actualState = undefined
    const expectState = initialState

    expect(reducers(actualState, action)).toEqual(expectState)
  })

  it(\`should handle case \${ACTION_TYPE_NAME.REQUEST}\`, () => {
    const action = { type: ACTION_TYPE_NAME.REQUEST }
    const actualState = undefined
    const expectState = {
      ...initialState,
      isFetching: true
    }

    expect(reducers(actualState, action)).toEqual(expectState)
  })

  it(\`should handle case \${ACTION_TYPE_NAME.SUCCESS}\`, () => {
    const action = {
      type: ACTION_TYPE_NAME.SUCCESS,
      code: mockResponse.success.data.code,
      data: mockResponse.success.data.data,
    }
    const actualState = initialState
    const expectState = {
      ...initialState,
      isFetching: initialState.isFetching,
      code: mockResponse.success.data.code,
      data: mockResponse.success.data.data,
    }

    expect(reducers(actualState, action)).toEqual(expectState)
  })

  it(\`should handle case \${ACTION_TYPE_NAME.FAILURE}\`, () => {
    const { error, code } = handleErrorResponse(mockResponse.failure)
    const action = {
      type: ACTION_TYPE_NAME.FAILURE,
      error,
      code,
    }
    const actualState = initialState
    const expectState = {
      ...initialState,
      isFetching: initialState.isFetching,
      error,
      code,
    }

    expect(reducers(actualState, action)).toEqual(expectState)
  })

  it(\`should clear all state when receive action type "\${LOG_OUT}"\`, () => {
    const action = { type: LOG_OUT }
    const actualState = {
      ...initialState,
      code: mockResponse.success.data.code,
      data: mockResponse.success.data.data,
    }
    const expectState = initialState

    expect(reducers(actualState, action)).toEqual(expectState)
  })
})

`