module.exports = `
import { project } from '@Configs'
import { selectREDUCER_NAME } from '../selectors'
import { initialState } from '../initialState'

const state = {
  [project.name]: {
    REDUCER_NAME: initialState
  }
}

describe('Selectors', () => {
  describe('selectREDUCER_NAME', () => {
    it('should be return an object of store, specific by name', () => {
      expect(selectREDUCER_NAME(state)).toEqual(initialState)
      expect(selectREDUCER_NAME(state, 'isFetching')).toEqual(initialState.isFetching)
      expect(selectREDUCER_NAME(state, 'data')).toEqual(initialState.data)
      expect(selectREDUCER_NAME(state, 'error')).toEqual(initialState.error)
      expect(selectREDUCER_NAME(state, 'code')).toEqual(initialState.code)
    })
  })
})

`