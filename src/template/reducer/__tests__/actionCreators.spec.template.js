module.exports = `
import axios from '../../../../../../__mocks__/axios'
import mockStore from '../../../../../config/mockStore'
import { ACTION_CREATOR_NAME } from './../actionCreators'
import { mock as mockResponse } from '../../../services/SERVICE_NAME'

const store = mockStore()

describe('REDUCER_NAME action creators', () => {
  beforeEach(() => {
    store.clearActions()
  })

  describe('ACTION_CREATOR_NAME action', () => {
    it('should be create an async action with handle response success', async () => {
      axios.mockResponseSuccess(mockResponse.success)
      await store.dispatch(ACTION_CREATOR_NAME())
      expect(store.getActions()).toMatchSnapshot()
    })

    it('should be handle and create an async action with handle response failure', async () => {
      axios.mockResponseFailure(mockResponse.failure)
      await store.dispatch(ACTION_CREATOR_NAME())
      expect(store.getActions()).toMatchSnapshot()
    })
  })
})
`