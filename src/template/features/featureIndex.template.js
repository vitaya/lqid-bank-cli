module.exports = (moduleName) => `export { ${moduleName}Routes } from './routes'
export { ${moduleName}Reducers } from './redux/reducers'`