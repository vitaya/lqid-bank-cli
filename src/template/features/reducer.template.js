module.exports = moduleName => `import { combineReducers } from 'redux'

const ${moduleName}Reducers = combineReducers({
  /* Add some reducer here */
})

export { ${moduleName}Reducers }`