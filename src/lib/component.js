const fs = require('fs')
const chalk = require('chalk')
const glob = require('glob')

const statelessComponentTemplate = require('../template/components/stlComponent.template')
const statefulComponentTemplate = require('../template/components/stfComponent.template')
const unitTestTemplate = require('../template/components/unit-test.template')
const styleTemplate = require('../template/components/style.template')

const componentGeneratingProcessLog = (word) => {
  console.log('  ' + chalk.bold(`🍔  ${word}`))
}

const convertTocamelCase = (string) => {
  const firstCharPattern = /(\w)(\w+)/g
  const firstString = string.replace(firstCharPattern, "$1").toLowerCase()
  const otherString = string.replace(firstCharPattern, "$2")
  return `${firstString}${otherString}`
}

const createFile = (path, data) => {
  fs.writeFile(path, data, (err) => {
    if (err) { return console.log(err); }
    componentGeneratingProcessLog(chalk.red('File: ') + path)
  });
}

const importExportTemplateGen = (moduleName, path) => {
  return `import ${moduleName} from '${path}'

export { ${moduleName} } 
  `
}

const createDir = (dir) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    componentGeneratingProcessLog(chalk.green('Directory: ') + dir)
  }
}

const generateComponent = (moduleName, isStateless, path) => {
  const createPath = path || ''
  const unitTestDir = `${moduleName}/__tests__`
  const stylesDir = `${moduleName}/stylesheets`

  const moduleNamePath = `${createPath}${moduleName}`
  const unitTestDirPath = `${createPath}${unitTestDir}`
  const stylesDirPath = `${createPath}${stylesDir}`

  const importStatement = `import { ${moduleName} } from './${moduleName}'\n$1`
  const exportStatement = `${moduleName},\n  $1`
  updateIndexFile(createPath, /(\/\/! auto-generator-import)/g, importStatement, () => {
    updateIndexFile(createPath, /(\/\/! auto-generator-export)/g, exportStatement, () => {
      componentGeneratingProcessLog(chalk.blue('Modify: index.js'))
    })
  })
  createDir(moduleNamePath)
  createDir(unitTestDirPath)
  createDir(stylesDirPath)

  const patternReplaceStyle = new RegExp(`${moduleName}Style`, 'g')
//   // Create Component File
  const addedModuleNameData = isStateless
    ? statelessComponentTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName).replace(patternReplaceStyle, `${convertTocamelCase(moduleName)}Style`)
    : statefulComponentTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName).replace(patternReplaceStyle, `${convertTocamelCase(moduleName)}Style`)
  createFile(`${moduleNamePath}/${moduleName}.js`, addedModuleNameData)
  createFile(`${moduleNamePath}/index.js`, importExportTemplateGen(moduleName, `./${moduleName}`))

  // Create Unit-test Files
  const addedModuleNameUnitTestData = unitTestTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName)
  createFile(`${unitTestDirPath}/${moduleName}.spec.js`, addedModuleNameUnitTestData)

  // Create style Files
  const addedModuleNameStyleData = styleTemplate
  createFile(`${stylesDirPath}/${convertTocamelCase(moduleName)}.style.js`, addedModuleNameStyleData)
  createFile(`${stylesDirPath}/index.js`, importExportTemplateGen(convertTocamelCase(moduleName) + 'Style', `./${convertTocamelCase(moduleName)}.style`))
}

const updateIndexFile = (path, aboveOfPattern, contentToAdd, cb) => {
  glob(`${path}index.js`, null, function (err, files) {
    const indexFile = files.filter(fileName => /^.*index.js$/.test(fileName))
    if (indexFile.length > 0) {
      fs.readFile(indexFile[0], (err, data) => {
        const indexFilesContent = data.toString()
        const addedContentData = indexFilesContent.replace(aboveOfPattern, `${contentToAdd}`)
        fs.writeFile(indexFile[0], addedContentData, (err) => {
          if (err) { return console.log(err); }
          cb()
        });
      })
    } else {
      componentGeneratingProcessLog(chalk.red('Cant Modify: index.js [Not found "index.js"]'))
    }

  })
}

const componentGenerator = (moduleName, isStateless, path) => {
  generateComponent(moduleName, isStateless, path)
}

module.exports = componentGenerator