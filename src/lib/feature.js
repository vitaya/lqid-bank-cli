const chalk = require('chalk')
const fileUtils = require('../utils/file')

const constantTemplate = require('../template/features/constant.template')
const utilsTemplate = require('../template/features/utils.template')
const transformTemplate = require('../template/features/transform.template')

const indexTemplate = require('../template/features/index.template')
const reducerTemplate = require('../template/features/reducer.template')
const routeTemplate = require('../template/features/routeIndex.template')
const featureIndexTemplate = require('../template/features/featureIndex.template')

const logFileTree = (module_name, path) => {
  console.log(chalk.bold.red("======================="))
  console.log(chalk.bold.red("+✨ GENERATED FILES ✨+"))
  console.log(chalk.bold.red("======================="))
  console.log(chalk.red(`\n${path}${module_name}
   |- assets
   |   |- images
   |     |- .gitkeep
   |- commons
   |   |- constants.js
   |   |- utils.js
   |   |- transforms.js
   |- components
   |   |- index.js
   |- containers
   |   |- index.js
   |- models
   |   |- index.js
   |- redux
   |   |- reducers.js
   |- routes
   |   |- index.js
   |- scenes
   |   |- index.js
   |- services
   |   |- endpoints.js
   |   |- index.js
   |- index.js`))
  console.log(chalk.bold.red("\n=======================\n"))
}

const generateFiles = (moduleName, path = '') => {
  // Directory
  const baseDir = `${path}${moduleName}`
  const assetsDir = `${baseDir}/assets`
  const imagesDir = `${assetsDir}/images`
  const commonsDir = `${baseDir}/commons`
  const componentsDir = `${baseDir}/components`
  const containersDir = `${baseDir}/containers`
  const modelsDir = `${baseDir}/models`
  const reduxDir = `${baseDir}/redux`
  const routesDir = `${baseDir}/routes`
  const scenesDir = `${baseDir}/scenes`
  const servicesDir = `${baseDir}/services`

  // Files
  const featureIndex = `${baseDir}/index.js`
  const common = {
    constant: `${commonsDir}/constants.js`,
    utils: `${commonsDir}/utils.js`,
    transform: `${commonsDir}/transforms.js`,
  }
  const component = {
    index: `${componentsDir}/index.js`
  }
  const container = {
    index: `${containersDir}/index.js`
  }
  const model = {
    index: `${modelsDir}/index.js`
  }
  const scene = {
    index: `${scenesDir}/index.js`
  }
  const reducer = {
    index: `${reduxDir}/reducers.js`
  }
  const routes = {
    index: `${routesDir}/index.js`
  }

  const services = {
    index: `${servicesDir}/index.js`
  }

  fileUtils.createDir(baseDir)
  fileUtils.createFile(featureIndex, featureIndexTemplate(moduleName))

  fileUtils.createDir(assetsDir)
  fileUtils.createDir(imagesDir)
  fileUtils.createDir(commonsDir)
  // Create Common Files
  fileUtils.createFile(common.constant, constantTemplate)
  fileUtils.createFile(common.utils, utilsTemplate)
  fileUtils.createFile(common.transform, transformTemplate)

  fileUtils.createDir(componentsDir)
  // Create Component Files
  fileUtils.createFile(component.index, indexTemplate)

  fileUtils.createDir(containersDir)
  // Create Container Files
  fileUtils.createFile(container.index, indexTemplate)

  fileUtils.createDir(modelsDir)
  // Create Model Files
  fileUtils.createFile(model.index, indexTemplate)

  fileUtils.createDir(reduxDir)
  // Create Reducer Files
  fileUtils.createFile(reducer.index, reducerTemplate(moduleName))

  fileUtils.createDir(routesDir)
  // Create Router Files
  fileUtils.createFile(routes.index, routeTemplate(moduleName))

  fileUtils.createDir(scenesDir)
  // Create Scene Files
  fileUtils.createFile(scene.index, indexTemplate)

  fileUtils.createDir(servicesDir)
  // Create Service Files
  fileUtils.createFile(services.index, indexTemplate)
}

const featureGenerator = (module_name, path) => {
  logFileTree(module_name, path)
  generateFiles(module_name, path)
}

module.exports = featureGenerator