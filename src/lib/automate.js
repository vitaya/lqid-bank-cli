#!/usr/bin/env node
import fs from 'fs'
import _capitalize from 'lodash/capitalize'
import _camelCase from 'lodash/camelCase'
const chalk = require('chalk');

const scenarioTemplate = require('../template/automates/featureScenario')
const scenarioHeaderTemplate = require('../template/automates/featureScenarioHeader')
const commonStepTemplate = require('../template/automates/commonStep')
const scenarioStepTemplate = require('../template/automates/scenarioStep')

const generatingDir = 'e2e'
const featureDir = `${generatingDir}/features`
const BDDFeature = `${featureDir}/bdd`
const stepDir = `${featureDir}/stepDefinitions`
const commonStepDir = `${stepDir}/common`
const scenarioStepDir = `${stepDir}/steps`

const automateProcessLog = (word) => {
  console.log('  ' + chalk.bold(`🌈 ${word}`))
}

const getStartLength = (data) => {
  const codePattern = /^BANK_(.*)_(.*)_(.*)_(.*)/g
  for (let index in data) {
    if (codePattern.test(data[index])) {
      return index
    }
  }
}

const findDuplicatedData = (data, key) => {
  let dupplicateList = []
  for (let index in data) {
    const dupplicatedData = data.filter((item, duppIndex) => {
      return item[key] === data[index][key] && index != duppIndex && !dupplicateList.includes(item[key])
    })
    if (dupplicatedData.length > 0) {
      dupplicateList = [...dupplicateList, dupplicatedData[0][key]]
    }
  }
  return dupplicateList
}


const createFeatureFile = (moduleName, featureTag, useStoryTag, data) => {
  fs.writeFile(`${BDDFeature}/${moduleName}/${featureTag}${useStoryTag}.feature`, data, (err) => {
    if (err) { return console.log(err); }
  });
}

const createCommonFile = (moduleName, data) => {
  fs.writeFile(`${commonStepDir}/${moduleName}.step.js`, data, (err) => {
    if (err) { return console.log(err); }
  });
}

const createScenarioStepFile = (moduleName, scenario, scenarioIndex, data) => {
  fs.writeFile(`${scenarioStepDir}/${moduleName}/${scenario}_${scenarioIndex}.step.js`, data, (err) => {
    if (err) { return console.log(err); }
  });
}

const extractDataFromSourceFile = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(`${path}`, 'utf8', function (err, contents) {
      if (!err) {
        const splittedData = contents.split(/\t/g)
        let currentIndex = 0
        const startAtIndex = getStartLength(splittedData)
        let f
        let u
        let scenarioWord

        const featureData = splittedData.reduce((prev, item, index) => {
          const codePattern = /^S(\d){3}$/g
          if (codePattern.test(item)) {
            scenarioWord = splittedData[index + 1]
          }
          if (index + 1 > startAtIndex) {
            const codePattern = /^BANK_(.*)_(.*)_(.*)_(.*)/g
            let given
            let when
            let then
            let s
            let sRow
            let tag
            if (codePattern.test(item)) {
              currentIndex = index
              f = item.replace(codePattern, "$1")
              u = item.replace(codePattern, "$2")
              s = item.replace(codePattern, "$3")
              sRow = item.replace(codePattern, "$4")
              tag = `${f}${u}${s}${sRow}`
            }
            if (currentIndex > 0) {
              const convertedNewLineGiven = splittedData[currentIndex + 2].replace(/\n|\n\r|\r/g, '')
              given = convertedNewLineGiven.replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim()

              const dataSet = splittedData[currentIndex + 3]

              // SplitKey
              const splittedDataset = dataSet.match(/([a-zA-Z0-9]+)(?:\s+)?=(?:\s+)?("[a-zA-Z0-9,\s\(\):-]+"|[a-zA-Z0-9.@+,-]+)/g)

              let exampleData = []
              if (splittedDataset) {
                for (let data of splittedDataset) {
                  const dataPattern = /(.*)(?:\s+)?=(?:\s+)?(.+)/g
                  exampleData = [
                    ...exampleData,
                    {
                      [data.replace(dataPattern, '$1')]: data.replace(dataPattern, '$2')
                    }
                  ]
                }
              }
              const convertedNewLineWhen = splittedData[currentIndex + 4].replace(/\n|\n\r|\r/g, '-')
              when = convertedNewLineWhen.replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim()
              const convertedNewLineThen = splittedData[currentIndex + 5].replace(/\n|\n\r|\r/g, '-')
              let showDataOnThen = []
              const splittedThen = convertedNewLineThen.split(/\[Show\]/g)
              then = splittedThen[0].replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim()
              if (splittedThen.length > 1) {
                showDataOnThen = splittedThen[1]
              }

              const and = then.split(/•/g).map(item => item.trim()).filter((_, index) => index > 1)
              then = then.split(/•/g).map(item => item.trim()).filter((_, index) => index > 0)[0] || then

              currentIndex = 0
              return [
                ...prev,
                {
                  feature: f,
                  userStory: u,
                  scenario: s,
                  scenarioIndex: sRow,
                  given,
                  when,
                  then,
                  and,
                  exampleData,
                  showDataOnThen,
                  scenarioWord,
                }
              ]
            }
          }
          return prev
        }, [])
        resolve(featureData)

      } else {
        reject('Error', err)
        console.error('Something went wrong!!', err)
      }
    });
  })
}

const createFeatureFromTemplate = async (moduleName, featureData) => {
  let baseFeature
  let baseUserStory
  const featureString = featureData.reduce((prev, current) => {
    const {
      feature,
      userStory,
      scenario,
      scenarioIndex,
      showDataOnThen,
      given,
      when,
      then,
      and,
      givenAnd,
      scenarioWord,
      exampleData,
    } = current
    if (!baseFeature) {
      baseFeature = feature
    }
    if (!baseUserStory) {
      baseUserStory = userStory
    }

    const expectedShowData = showDataOnThen.length > 0
      ? showDataOnThen
        .split('•')
        .filter(item => item.trim() !== '')
        .map(item => item.trim())
        .map(item => {
          const dataPattern = /(.*)\s*=\s*(.*)/g
          const key = _capitalize(item.replace(dataPattern, '$1')
            .trim()
            .replace(/\s/g, '_'))
          const value = item.replace(dataPattern, '$2')
          return ({
            [key]: value
          })
        })
      : []

    const generateTable = (datas) => {
      const header = datas.reduce((prev, current, _) => {
        const key = Object.keys(current)[0]
        return `${prev} ${key} |`
      }, '|')
      const dataItem = datas.reduce((prev, current, _) => {
        const value = Object.values(current)[0]
        return `${prev} ${value} |`
      }, '|')
      return `${header}\n        ${dataItem}`
    }

    let trutlyThen = `${then}${showDataOnThen.length > 0 ? `\n${generateTable(expectedShowData)}` : ''}`
    if (and.length > 0) {
      const combinedAnd = and.reduce((prev, current) => (`${prev}\n    And ${current}`), '')
      trutlyThen = `${then}${showDataOnThen.length > 0 ? `\n        ${generateTable(expectedShowData)}` : ''}${combinedAnd}`
    }
    let trutlyGiven = given
    if (givenAnd.length > 0) {
      const combinedAnd = givenAnd.reduce((prev, current) => (`${prev}\n    And ${current}`), '')
      trutlyGiven = `${given}${exampleData.length > 0 ? `\n        ${generateTable(exampleData)}` : ''}${combinedAnd}`
    }
    const tag = `${feature}${userStory}${scenario}${scenarioIndex}`
    const replacedTemplate = scenarioTemplate
      .replace(/__%TAG%__/g, tag)
      .replace(/__%MODULE_NAME%__/g, moduleName)
      .replace(/__%FEATURE_NAME%__/g, moduleName)
      .replace(/__%SCENARIO_NUMBER%__/g, `${scenario}_${scenarioIndex}`)
      .replace(/__%SCENARIO_WORD%__/g, scenarioWord)
      .replace(/__%GIVEN%__/g, `${trutlyGiven.trim()}`)
      .replace(/__%WHEN%__/g, when.trim())
      .replace(/__%THEN%__/g, trutlyThen.trim())

    return prev + replacedTemplate
  }, '')
  createFeatureFile(moduleName, baseFeature, baseUserStory, scenarioHeaderTemplate + featureString)
}

const createCommonFromTemplate = async (moduleName, dupplicatedData) => {
  const replacedDataWhen = dupplicatedData.when.reduce((prev, item) => prev + commonStepTemplate.when.replace('__%WHEN%__', item), '')
  const replacedDataGiven = dupplicatedData.given.reduce((prev, item) => prev + commonStepTemplate.given.replace('__%GIVEN%__', item), '')
  const replacedDataThen = dupplicatedData.then.reduce((prev, item) => prev + commonStepTemplate.then.replace('__%THEN%__', item), '')
  const replacedDataAnd = dupplicatedData.and.reduce((prev, item) => prev + commonStepTemplate.then.replace('__%THEN%__', item.replace(/\//g, '\\/').replace(/\(/g, '\\(').replace(/\)/g, '\\)')), '')
  const replacedDataGivenAnd = dupplicatedData.givenAnd.reduce((prev, item) => prev + commonStepTemplate.then.replace('__%THEN%__', item.replace(/\//g, '\\/').replace(/\(/g, '\\(').replace(/\)/g, '\\)')), '')

  createCommonFile(moduleName,
    "import { Given, When, Then } from 'cucumber'\n\n" +
    '\n// Auto-generate Given \n\n' + replacedDataGiven +
    '\n// Auto-generate Given-And \n\n' + replacedDataGivenAnd +
    '\n// Auto-generate When \n\n' + replacedDataWhen +
    '\n // Auto-generate Then \n\n' + replacedDataThen +
    '\n// Auto-generate And \n\n' + replacedDataAnd
  )
}

const getComplatibleQuote = (data) => {
  const singleQuotePattern = /'/g
  const doubleQuotePattern = /"/g
  const foundSingleQuote = singleQuotePattern.test(data)
  const foundDoubleQuote = doubleQuotePattern.test(data)
  if (foundDoubleQuote && foundSingleQuote) {
    return "`"
  } else if (foundSingleQuote) {
    return "\""
  }
  return "'"
}

const createScenarioStepFromTemplate = async (moduleName, data, dupplicatedData) => {
  for (let scenarioItem of data) {
    const extendedGiven = `${scenarioItem.given}`
    const replaceEscapeCharacter = {
      given: extendedGiven.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
      when: scenarioItem.when.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
      then: scenarioItem.then.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
    }

    const replacedData = scenarioStepTemplate
      .replace("__%GIVEN%__", `${replaceEscapeCharacter.given}`)
      .replace("__%WHEN%__", `${getComplatibleQuote(replaceEscapeCharacter.when)}${replaceEscapeCharacter.when}${getComplatibleQuote(replaceEscapeCharacter.when)}`)
      .replace("__%THEN%__", `${getComplatibleQuote(replaceEscapeCharacter.then)}${replaceEscapeCharacter.then}${getComplatibleQuote(replaceEscapeCharacter.then)}`)
      .replace(/__%SLASH%__/g, "\\/")

    const removedDupplicateWhen = dupplicatedData.when.includes(scenarioItem.when)
      ? replacedData.replace(/When\((.*)}\)/g, `// ! WHEN - "${scenarioItem.when}" IS DUPLICATED!! It will be at common step.`)
      : replacedData
    const removedDupplicateGiven = dupplicatedData.given.includes(scenarioItem.given)
      ? removedDupplicateWhen.replace(/Given\((.*)\n.*\n.*\n.*\n.*\n.*\n.*\n}\)/g, `// ! GIVEN - "${scenarioItem.given}" IS DUPLICATED!! It will be at common step.`)
      : removedDupplicateWhen
    const removedDupplicateThen = dupplicatedData.then.includes(scenarioItem.then)
      ? removedDupplicateGiven.replace(/Then\((.*)}\)/g, `// ! THEN - "${scenarioItem.then}" IS DUPLICATED!! It will be at common step.`)
      : removedDupplicateGiven

    // Then And Logic
    let trutlyDupllicated = ''

    if (dupplicatedData.and.length > 0) {
      const escapedCharAdd = scenarioItem.and.map(addItem => {
        return addItem.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)')
      })

      trutlyDupllicated = escapedCharAdd.reduce((prev, current, index) => {
        const thenStatement = (sentense) => (`\n\nThen(${getComplatibleQuote(sentense)}${sentense}${getComplatibleQuote(sentense)}, async () => { /* TODO: ADD STEP HERE */ })`)
        const removeDuplicateAnd = dupplicatedData.and.includes(scenarioItem.and[index])
          ? `\n\n// ! AND - "${current}" IS DUPLICATED!! It will be at common step.`
          : thenStatement(current).replace(/__%SLASH%__/g, "\\/")
        return `${prev}${removeDuplicateAnd}`
      }, '')
    }

    const formattedData = `${removedDupplicateThen}${trutlyDupllicated}`.replace(/(async \(\) => {)(.*)(}\))/g, '$1\n$2\n$3')

    // Given And Logic
    let trutlyDupllicatedGivenAnd = ''

    if (dupplicatedData.givenAnd.length > 0) {
      const escapedCharAdd = scenarioItem.givenAnd.map(addItem => {
        return addItem.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)')
      })

      trutlyDupllicatedGivenAnd = escapedCharAdd.reduce((prev, current, index) => {
        const thenStatement = (sentense) => (`\n\nThen(${getComplatibleQuote(sentense)}${sentense}${getComplatibleQuote(sentense)}, async () => { /* TODO: ADD STEP HERE */ })`)

        const removeDuplicateAnd = dupplicatedData.givenAnd.includes(scenarioItem.givenAnd[index])
          ? `\n\n// ! Given-And - "${current}" IS DUPLICATED!! It will be at common step.`
          : thenStatement(current).replace(/__%SLASH%__/g, "\\/")
        return `${prev}${removeDuplicateAnd}`
      }, '')
    }

    const formattedData2 = formattedData.replace(/__%GIVEN_AND%__/, trutlyDupllicatedGivenAnd).replace(/(async \(\) => {)(.*)(}\))/g, '$1\n$2\n$3')
    createScenarioStepFile(moduleName, scenarioItem.scenario, scenarioItem.scenarioIndex, formattedData2)
  }
}

const validatingDirectory = (moduleName) => new Promise((resolve, reject) => {
  try {
    if (!fs.existsSync(generatingDir)) {
      fs.mkdirSync(generatingDir);
      automateProcessLog('Created Directory >> ' + generatingDir)
    }
    if (!fs.existsSync(featureDir)) {
      fs.mkdirSync(featureDir);
      automateProcessLog('Created Directory >> ' + featureDir)
    }
    if (!fs.existsSync(BDDFeature)) {
      fs.mkdirSync(BDDFeature);
      automateProcessLog('Created Directory >> ' + BDDFeature)
    }
    if (!fs.existsSync(stepDir)) {
      fs.mkdirSync(stepDir);
      automateProcessLog('Created Directory >> ' + stepDir)
    }
    if (!fs.existsSync(commonStepDir)) {
      fs.mkdirSync(commonStepDir);
      automateProcessLog('Created Directory >> ' + commonStepDir)
    }
    if (!fs.existsSync(scenarioStepDir)) {
      fs.mkdirSync(scenarioStepDir);
      automateProcessLog('Created Directory >> ' + scenarioStepDir)
    }
    const moduleFolder = `${BDDFeature}/${moduleName}`
    const moduleStepFolder = `${scenarioStepDir}/${moduleName}`
    if (!fs.existsSync(moduleFolder)) {
      fs.mkdirSync(moduleFolder);
      automateProcessLog('Created Directory >> ' + moduleFolder)
    }
    if (!fs.existsSync(moduleStepFolder)) {
      fs.mkdirSync(moduleStepFolder);
      automateProcessLog('Created Directory >> ' + moduleStepFolder)
    }
    resolve()
  } catch (error) {
    reject(error)
  }
})

const runJob = async (moduleName, path) => {
  try {
    await validatingDirectory(moduleName)
    const rawData = await extractDataFromSourceFile(path)
    automateProcessLog(` Total Scenario: \t` + chalk.green(rawData.length))

    const data = rawData.map((dataItem) => {
      const splittedGiven = dataItem.given.split(/and/)
      const given = splittedGiven[0].trim()
      const givenAnd = splittedGiven.filter((_, index) => index > 0).map(item => item.trim())
      return ({
        ...dataItem,
        given,
        givenAnd,
      })
    })

    const dupplicatedGiven = findDuplicatedData(data, 'given')
    console.log(chalk.bold(`\n  🌈  Duplicate Given: \t\t${dupplicatedGiven.length}`))

    const givenAndCase = data
      .reduce((prev, current) => ([...prev, ...current.givenAnd]), [])
      .reduce((prev, current) => (prev.includes(current) ? prev : [...prev, { givenAnd: current }]), [])
    const dupplicatedGivenAnd = findDuplicatedData(givenAndCase, 'givenAnd').filter((item) => item !== '')
    console.log(chalk.bold(`  🌈  Duplicate Given-And: \t${dupplicatedGivenAnd.length}`))

    const dupplicatedWhen = findDuplicatedData(data, 'when')
    console.log(chalk.bold(`  🌈  Duplicate When: \t\t${dupplicatedWhen.length}`))

    const dupplicatedThen = findDuplicatedData(data, 'then')
    console.log(chalk.bold(`  🌈  Duplicate Then: \t\t${dupplicatedThen.length}`))

    const andCase = data
      .reduce((prev, current) => ([...prev, ...current.and]), [])
      .reduce((prev, current) => (prev.includes(current) ? prev : [...prev, { and: current }]), [])
    const dupplicatedAnd = findDuplicatedData(andCase, 'and')
    console.log(chalk.bold(`  🌈  Duplicate ThenAnd: \t${dupplicatedAnd.length}\n`))

    await createFeatureFromTemplate(moduleName, data)
    await createCommonFromTemplate(moduleName, {
      given: dupplicatedGiven,
      givenAnd: dupplicatedGivenAnd,
      then: dupplicatedThen,
      when: dupplicatedWhen,
      and: dupplicatedAnd,
    })
    await createScenarioStepFromTemplate(moduleName, data, {
      given: dupplicatedGiven,
      givenAnd: dupplicatedGivenAnd,
      then: dupplicatedThen,
      when: dupplicatedWhen,
      and: dupplicatedAnd,
    })

    console.log(chalk.bold(`\n✨ Success ✨\n`))

  } catch (error) {
    console.error(error)
  }
}

module.exports = runJob
