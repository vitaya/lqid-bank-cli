const chalk = require('chalk');
const { upperFirst } = require('lodash');
const fs = require('fs');
const shell = require('child_process').execSync
const inquirer = require('inquirer');
const { PathPrompt } = require('inquirer-path');
const autoCompletePathPrompt = require('inquirer-autocomplete-prompt');
const { createFile, createDir, } = require('../utils/file');
const searchChoice = require('../utils/searchChoice')
const {
  replaceWording,
  getContainerExport,
  autoExportImportModule,
  autoImport,
} = require('../utils/modules')
const sceneTemplate = require('../template/scenes/sceneName.scene.template')
const indexTemplate = require('../template/index.template')

inquirer.registerPrompt('path', PathPrompt);
inquirer.registerPrompt('autocomplete', autoCompletePathPrompt);

// TODO smart check with dup path
function execute(module_name, path) {
  if (path) {
    throw new Error('custom path is not support this feature')
  }

  const featuresDir = 'src/features'
  const featureList = fs.readdirSync(featuresDir)

  inquirer.prompt([
    {
      type: 'autocomplete',
      name: 'feature',
      message: 'Select a feature',
      source: searchChoice(featureList),
    },
    {
      type: 'autocomplete',
      name: 'container',
      message: 'Select a container',
      source: (answers, input = '') => {
        const containerPath = `${featuresDir}/${answers.feature}/containers`
        const containerList = getContainerExport(containerPath)

        return searchChoice(containerList)(answers, input)
      }
    }
  ]).then(function(answers) {
    // const containerPath = `${featuresDir}/${answers.feature}/containers/${answers.container}`
    // const fileContentContainer = fs.readFileSync(containerPath, { encoding: 'utf8' })
    // console.log('fileContentContainer', fileContentContainer)
    const sceneName = `${upperFirst(module_name)}Scene`
    const containerName = answers.container

    const fileContent = replaceWording(sceneTemplate, {
      '__%CONTAINER_NAME%__': containerName,
      '__%SCENE_NAME%__': sceneName,
    })

    const featurePath = `${featuresDir}/${answers.feature}`
    const scenePath = `${featurePath}/scenes`
    const routePath = `${featurePath}/routes`
    const sceneIndexPath = `${scenePath}/index.js`
    const routeIndexPath = `${routePath}/index.js`
    const sceneFileName = `${module_name}.scene.js`
    const sceneFilePath = `${scenePath}/${sceneFileName}`
    const routeProps = `  ${module_name}: {
    key: '${module_name}',
    component: ${sceneName},
    hideNavBar: true,
    initial: false,
  },`

    // create folder scene
    createDir(scenePath)
    // create index.js of scene
    if (!fs.existsSync(sceneIndexPath)) {
      createFile(sceneIndexPath, indexTemplate)
    }

    // import/export /scene/index.js
    const autoImportScene = autoExportImportModule(sceneName, `./${sceneFileName}`, sceneIndexPath, true)
    const autoImportRoute = autoImport(sceneName, `../scenes`, routeIndexPath)
    const autoAddRount = autoImportRoute.split('\n').reduce((prev, cur) => {
      if (cur.trim() === '})') {
        return [
          ...prev,
          routeProps,
          cur
        ]
      }
      return [...prev, cur]
    }, []).join('\n')

    // clear file scene
    createFile(sceneFilePath, fileContent)

    // auto import scene
    createFile(sceneIndexPath, autoImportScene)

    // auto import route
    createFile(routeIndexPath, autoAddRount)

    chalk.green('Generate scene successful!!!')
  });
}

module.exports = execute