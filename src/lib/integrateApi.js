const program = require('commander');
const chalk = require('chalk');
const { upperFirst } = require('lodash');
const fs = require('fs');
const shell = require('child_process').execSync
const inquirer = require('inquirer');
const { PathPrompt } = require('inquirer-path');
const { createFile, createDir, } = require('../utils/file');

inquirer.registerPrompt('path', PathPrompt);

// redux
const indexTemplate = require('../template/reducer/index.template')
const actionTypesTemplate = require('../template/reducer/actionTypes.template')
const actionCreatorsTemplate = require('../template/reducer/actionCreators.template')
const reducersTemplate = require('../template/reducer/reducers.template')
const initialStateTemplate = require('../template/reducer/initialState.template')
const selectorsTemplate = require('../template/reducer/selectors.template')

// unit test redux
const unitTestReducersTemplate = require('../template/reducer/__tests__/reducers.spec.template')
const unitTestActionCreatorsTemplate = require('../template/reducer/__tests__/actionCreators.spec.template')
const unitTestSelectorsTemplate = require('../template/reducer/__tests__/selectors.spec.template')

/**
 * Defined variable command
 * --
 */
const SERVICE_NAME        = 'SERVICE_NAME'
const ENDPOINT            = 'ENDPOINT'
const REDUCER_NAME        = 'REDUCER_NAME'
const ACTION_TYPE_NAME    = 'ACTION_TYPE_NAME'
const ACTION_CREATOR_NAME = 'ACTION_CREATOR_NAME'
const FEATURE_PATH        = 'FEATURE_PATH'
let   IMPORT_SERVICE_PATH = '' // TODO Gen. Service function

function replaceWording(pathFile, fileContent, wording = {}) {
  // Replace wording in file
  Object.entries(wording).map(([key, value]) => {
    const pattern = new RegExp(key, 'g')
    fileContent = fileContent.replace(pattern, value)
  })

  // Write file with include new wording
  fs.writeFileSync(pathFile, fileContent, { encoding: 'utf8' })
}

// TODO Gen. Service
// TODO Plugin reducers
// TODO log task
// TODO with Sub reducer ?
// TODO Selector name
function execute() {
  inquirer
    .prompt([
      { type: 'input', name: SERVICE_NAME,        message: 'Enter service name?', suffix: ' (ex. getAccountDetail) :' },
      // { type: 'input', name: ENDPOINT,            message: 'Enter your endpoint name?' },
      { type: 'input', name: REDUCER_NAME,        message: 'Enter reducer name?', suffix: ' (ex. accountDetail) :' },
      { type: 'input', name: ACTION_CREATOR_NAME, message: 'Enter actionCreator name?', suffix: ' (ex. getAccountDetail) :' },
      { type: 'input', name: ACTION_TYPE_NAME,    message: 'Enter actionType name?', suffix: ' (ex. GET_ACCOUNT_DETAIL) :' },
      { type: 'path',  name: FEATURE_PATH,        message: 'Enter feature path',  suffix: ' (optional) :'  }
    ])
    .then(answers => {
      const rootReduxPath = `${answers[FEATURE_PATH]}/redux`
      const reduxPath = `${rootReduxPath}/${answers[REDUCER_NAME]}`

      // Path file
      const indexPath = `${reduxPath}/index.js`
      const actionCreatorsPath = `${reduxPath}/actionCreators.js`
      const actionTypesPath = `${reduxPath}/actionTypes.js`
      const reducersPath = `${reduxPath}/reducers.js`
      const selectorsPath = `${reduxPath}/selectors.js`
      const initialStatePath = `${reduxPath}/initialState.js`
      IMPORT_SERVICE_PATH = `${answers[FEATURE_PATH]}/services`

      // Unit test
      const unitTestActionCreatorsPath = `${reduxPath}/__tests__/actionCreators.spec.js`
      const unitTestReducerPath = `${reduxPath}/__tests__/reducers.spec.js`
      const unitTestSelectorPath = `${reduxPath}/__tests__/selectors.spec.js`

      // Create dir
      createDir(rootReduxPath)
      createDir(reduxPath)
      createDir(`${reduxPath}/__tests__`)

      // index
      replaceWording(indexPath, indexTemplate, {
        ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME],
        REDUCER_NAME: answers[REDUCER_NAME],
        [`select${answers[REDUCER_NAME]}`]: `select${upperFirst(answers[REDUCER_NAME])}`,
      })

      // initialState
      createFile(initialStatePath, initialStateTemplate)

      // actionTypes
      replaceWording(actionTypesPath, actionTypesTemplate, {
        ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
      })

      // reducers
      replaceWording(reducersPath, reducersTemplate, {
        ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
        REDUCER_NAME: answers[REDUCER_NAME],
      })
      // [Unit test] reducers
      replaceWording(unitTestReducerPath, unitTestReducersTemplate, {
        SERVICE_NAME: answers[SERVICE_NAME],
        ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
        REDUCER_NAME: upperFirst(answers[REDUCER_NAME]),
      })

      // actionCreators
      replaceWording(actionCreatorsPath, actionCreatorsTemplate, {
        SERVICE_NAME: answers[SERVICE_NAME],
        // IMPORT_SERVICE_PATH: IMPORT_SERVICE_PATH,
        ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
        ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME],
      })
      // [Unit test] actionCreators
      replaceWording(unitTestActionCreatorsPath, unitTestActionCreatorsTemplate, {
        SERVICE_NAME: answers[SERVICE_NAME],
        REDUCER_NAME: upperFirst(answers[REDUCER_NAME]),
        // IMPORT_SERVICE_PATH: IMPORT_SERVICE_PATH,
        ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME],
      })

      // selectorsPath
      replaceWording(selectorsPath, selectorsTemplate, {
        REDUCER_NAME: answers[REDUCER_NAME],
        [`select${answers[REDUCER_NAME]}`]: `select${upperFirst(answers[REDUCER_NAME])}`,
      })
      // [Unit test] selectorsPath
      replaceWording(unitTestSelectorPath, unitTestSelectorsTemplate, {
        REDUCER_NAME: answers[REDUCER_NAME],
        [`select${answers[REDUCER_NAME]}`]: `select${upperFirst(answers[REDUCER_NAME])}`,
      })
    });
}

module.exports = execute