const fs = require('fs');

function isDirSupportIndexExport(path) {
  if (fs.existsSync(path)) {
    const hasExport = fs.readFileSync(path, 'utf8').includes('export')
    return hasExport
  } else {
    return false
  }
}


function getModuleExport(path) {
  if (fs.existsSync(path)) {
    const fileContent = fs.readFileSync(path, 'utf8')
    const [, moduleExport] = fileContent.split('export')
    return moduleExport
    .replace(/\n|{|}|\s/g, '')
    .replace('/**Addsomemodulehere*/', '')
    .replace('//!auto-generator-', '')
    .split(',')
    .filter(Boolean)
  } else {
    return []
  }
}

function getContainerExport(path, flag = true) {
  if (isDirSupportIndexExport(`${path}/index.js`)) {
    return getModuleExport(`${path}/index.js`)
  } else if (flag) {
    const containerList = fs.readdirSync(path)
    return containerList.map(conPath => getContainerExport(`${path}/${conPath}`, false))
  }
}

function replaceWording(fileContent, wording = {}) {
  // Replace wording in file
  Object.entries(wording).map(([key, value]) => {
    const pattern = new RegExp(key, 'g')
    fileContent = fileContent.replace(pattern, value)
  })
  return fileContent
}


function isSupportCommentAutoGenerator(fileContent) {
  return (
    fileContent.includes('auto-generator-import')
      && fileContent.includes('auto-generator-import')
  )
}

function getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault) {
  if (isImportDefault) {
    return `import ${moduleName} from '${modulePath.replace('.js', '')}'`
  }

  return `import { ${moduleName} } from '${modulePath.replace('.js', '')}'`
}

function autoImport(moduleName, modulePath, targetPath, isImportDefault = false) {
  const fileContent = fs.readFileSync(targetPath, { encoding: 'utf8' })

  // case has comment auto-generator-import
  if (isSupportCommentAutoGenerator(fileContent)) {
    const importText = getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault)
    const importTextReplace = `${importText}\n//! auto-generator-import`

    return fileContent.replace('//! auto-generator-import', importTextReplace)
  }

  const importText = getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault)
  const importTextReplace = `${importText}\n//! auto-generator-import\n`

  return [
    importTextReplace,
    ...fileContent.split('\n'),
  ].join('\n')
}

function autoExportImportModule(moduleName, modulePath, indexPath, isImportDefault = false) {
  const fileContentIndex = fs.readFileSync(indexPath, { encoding: 'utf8' })
  let importText = getSelectImportModule(fileContentIndex, moduleName, modulePath, isImportDefault)

  const exportText = `${moduleName}`

  if (isSupportCommentAutoGenerator(fileContentIndex)) {
    const importTextReplace = `${importText}\n//! auto-generator-import`
    const exportTextReplace = `${exportText},\n  //! auto-generator-export`

    const finallyFileContent = fileContentIndex
    .replace('//! auto-generator-import', importTextReplace)
    .replace('//! auto-generator-export', exportTextReplace)

    return finallyFileContent
  } else {
    let finallyFileContent = ''
    const [importContent, exportContent] = fileContentIndex.split('export')

    const finallExportContent = [
      '\n',
      'export {',
      ...exportContent.replace(/\n|{|}|\s/g, '')
        .split(',')
        .filter(Boolean)
        .concat([exportText]).map(v => `  ${v},`),
      '}',
    ].join('\n')

    finallyFileContent = importContent
      .split('\n')
      .filter(Boolean)
      .concat([importText], finallExportContent)
      .join('\n')

    return finallyFileContent
  }
}

module.exports = {
  isDirSupportIndexExport,
  getModuleExport,
  getContainerExport,
  replaceWording,
  autoExportImportModule,
  autoImport,
}