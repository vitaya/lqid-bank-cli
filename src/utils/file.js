const fs = require('fs')

const createFile = (path, data) => {
  fs.writeFile(path, data, (err) => {
    if (err) { return console.log(err); }
    if (typeof callback === 'function') {
      callback()
    }
  });
}

const createDir = (dir, callback = () => { }) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    if (typeof callback === 'function') {
      callback()
    }
  }
}

module.exports = {
  createFile,
  createDir,
}