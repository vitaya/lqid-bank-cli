
function searchChoice(choices) {
  return (answers, input = '') => {
    return new Promise(function(resolve) {
      const choicesFilter = choices.filter(v => v.includes(input))
      resolve(
        choicesFilter.length === 0 ? [input] : choicesFilter
      )
    })
  }
}

module.exports = searchChoice
