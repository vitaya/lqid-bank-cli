#!/usr/bin/env node
import "@babel/polyfill";

const app = require('commander');
const chalk = require('chalk');

const automateGenerateJob = require('./lib/automate')
const componentGenerator = require('./lib/component')
const featureGenerator = require('./lib/feature')
const sceneGenerator = require('./lib/scene')
const integrateAPI = require('./lib/integrateAPI')

app
  .version('0.1.2')

app
  .command('generate <type> <module_name> [path]')
  .alias('gen')
  .description('Generating files follow by argument [automate] ')
  .action(function (type, module_name, path, cmd) {
    if (type === 'automate') {
      console.log(chalk.bold(`\n✨ Generating ${chalk.green(module_name)}'s automate files ✨\n`))
      automateGenerateJob(module_name, path)
    } else if (type === 'stl-component') {
      console.log(chalk.bold(`\n✨ Generating ${chalk.green(module_name)} Stateless Component ✨\n`))
      componentGenerator(module_name, true, path)
    } else if (type === 'stf-component') {
      console.log(chalk.bold(`\n✨ Generating ${chalk.green(module_name)} Stateful Component ✨\n`))
      componentGenerator(module_name, false, path)
    } else if (type === 'feature') {
      console.log(chalk.bold(`\n✨ Generating ${chalk.green(module_name)} Feature ✨\n`))
      featureGenerator(module_name, path)
    } else if (type === 'scene') {
      console.log(chalk.bold(`\n✨ Generating ${chalk.green(module_name)} Scene ✨\n`))
      sceneGenerator(module_name, path)
    } else {
      console.error('Required some argument')
    }
  })

app
  .command('integrateAPI')
  .alias('intAPI')
  .action(function () {
    integrateAPI()
  })

app.parse(process.argv);