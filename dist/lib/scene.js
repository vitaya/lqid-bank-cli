"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var chalk = require('chalk');

var _require = require('lodash'),
    upperFirst = _require.upperFirst;

var fs = require('fs');

var shell = require('child_process').execSync;

var inquirer = require('inquirer');

var _require2 = require('inquirer-path'),
    PathPrompt = _require2.PathPrompt;

var autoCompletePathPrompt = require('inquirer-autocomplete-prompt');

var _require3 = require('../utils/file'),
    createFile = _require3.createFile,
    createDir = _require3.createDir;

var searchChoice = require('../utils/searchChoice');

var _require4 = require('../utils/modules'),
    replaceWording = _require4.replaceWording,
    getContainerExport = _require4.getContainerExport,
    autoExportImportModule = _require4.autoExportImportModule,
    autoImport = _require4.autoImport;

var sceneTemplate = require('../template/scenes/sceneName.scene.template');

var indexTemplate = require('../template/index.template');

inquirer.registerPrompt('path', PathPrompt);
inquirer.registerPrompt('autocomplete', autoCompletePathPrompt); // TODO smart check with dup path

function execute(module_name, path) {
  if (path) {
    throw new Error('custom path is not support this feature');
  }

  var featuresDir = 'src/features';
  var featureList = fs.readdirSync(featuresDir);
  inquirer.prompt([{
    type: 'autocomplete',
    name: 'feature',
    message: 'Select a feature',
    source: searchChoice(featureList)
  }, {
    type: 'autocomplete',
    name: 'container',
    message: 'Select a container',
    source: function source(answers) {
      var input = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var containerPath = "".concat(featuresDir, "/").concat(answers.feature, "/containers");
      var containerList = getContainerExport(containerPath);
      return searchChoice(containerList)(answers, input);
    }
  }]).then(function (answers) {
    // const containerPath = `${featuresDir}/${answers.feature}/containers/${answers.container}`
    // const fileContentContainer = fs.readFileSync(containerPath, { encoding: 'utf8' })
    // console.log('fileContentContainer', fileContentContainer)
    var sceneName = "".concat(upperFirst(module_name), "Scene");
    var containerName = answers.container;
    var fileContent = replaceWording(sceneTemplate, {
      '__%CONTAINER_NAME%__': containerName,
      '__%SCENE_NAME%__': sceneName
    });
    var featurePath = "".concat(featuresDir, "/").concat(answers.feature);
    var scenePath = "".concat(featurePath, "/scenes");
    var routePath = "".concat(featurePath, "/routes");
    var sceneIndexPath = "".concat(scenePath, "/index.js");
    var routeIndexPath = "".concat(routePath, "/index.js");
    var sceneFileName = "".concat(module_name, ".scene.js");
    var sceneFilePath = "".concat(scenePath, "/").concat(sceneFileName);
    var routeProps = "  ".concat(module_name, ": {\n    key: '").concat(module_name, "',\n    component: ").concat(sceneName, ",\n    hideNavBar: true,\n    initial: false,\n  },"); // create folder scene

    createDir(scenePath); // create index.js of scene

    if (!fs.existsSync(sceneIndexPath)) {
      createFile(sceneIndexPath, indexTemplate);
    } // import/export /scene/index.js


    var autoImportScene = autoExportImportModule(sceneName, "./".concat(sceneFileName), sceneIndexPath, true);
    var autoImportRoute = autoImport(sceneName, "../scenes", routeIndexPath);
    var autoAddRount = autoImportRoute.split('\n').reduce(function (prev, cur) {
      if (cur.trim() === '})') {
        return [].concat(_toConsumableArray(prev), [routeProps, cur]);
      }

      return [].concat(_toConsumableArray(prev), [cur]);
    }, []).join('\n'); // clear file scene

    createFile(sceneFilePath, fileContent); // auto import scene

    createFile(sceneIndexPath, autoImportScene); // auto import route

    createFile(routeIndexPath, autoAddRount);
    chalk.green('Generate scene successful!!!');
  });
}

module.exports = execute;