#!/usr/bin/env node
"use strict";

var _fs = _interopRequireDefault(require("fs"));

var _capitalize2 = _interopRequireDefault(require("lodash/capitalize"));

var _camelCase2 = _interopRequireDefault(require("lodash/camelCase"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var chalk = require('chalk');

var scenarioTemplate = require('../template/automates/featureScenario');

var scenarioHeaderTemplate = require('../template/automates/featureScenarioHeader');

var commonStepTemplate = require('../template/automates/commonStep');

var scenarioStepTemplate = require('../template/automates/scenarioStep');

var generatingDir = 'e2e';
var featureDir = "".concat(generatingDir, "/features");
var BDDFeature = "".concat(featureDir, "/bdd");
var stepDir = "".concat(featureDir, "/stepDefinitions");
var commonStepDir = "".concat(stepDir, "/common");
var scenarioStepDir = "".concat(stepDir, "/steps");

var automateProcessLog = function automateProcessLog(word) {
  console.log('  ' + chalk.bold("\uD83C\uDF08 ".concat(word)));
};

var getStartLength = function getStartLength(data) {
  var codePattern = /^BANK_(.*)_(.*)_(.*)_(.*)/g;

  for (var index in data) {
    if (codePattern.test(data[index])) {
      return index;
    }
  }
};

var findDuplicatedData = function findDuplicatedData(data, key) {
  var dupplicateList = [];

  var _loop = function _loop(index) {
    var dupplicatedData = data.filter(function (item, duppIndex) {
      return item[key] === data[index][key] && index != duppIndex && !dupplicateList.includes(item[key]);
    });

    if (dupplicatedData.length > 0) {
      dupplicateList = [].concat(_toConsumableArray(dupplicateList), [dupplicatedData[0][key]]);
    }
  };

  for (var index in data) {
    _loop(index);
  }

  return dupplicateList;
};

var createFeatureFile = function createFeatureFile(moduleName, featureTag, useStoryTag, data) {
  _fs.default.writeFile("".concat(BDDFeature, "/").concat(moduleName, "/").concat(featureTag).concat(useStoryTag, ".feature"), data, function (err) {
    if (err) {
      return console.log(err);
    }
  });
};

var createCommonFile = function createCommonFile(moduleName, data) {
  _fs.default.writeFile("".concat(commonStepDir, "/").concat(moduleName, ".step.js"), data, function (err) {
    if (err) {
      return console.log(err);
    }
  });
};

var createScenarioStepFile = function createScenarioStepFile(moduleName, scenario, scenarioIndex, data) {
  _fs.default.writeFile("".concat(scenarioStepDir, "/").concat(moduleName, "/").concat(scenario, "_").concat(scenarioIndex, ".step.js"), data, function (err) {
    if (err) {
      return console.log(err);
    }
  });
};

var extractDataFromSourceFile = function extractDataFromSourceFile(path) {
  return new Promise(function (resolve, reject) {
    _fs.default.readFile("".concat(path), 'utf8', function (err, contents) {
      if (!err) {
        var splittedData = contents.split(/\t/g);
        var currentIndex = 0;
        var startAtIndex = getStartLength(splittedData);
        var f;
        var u;
        var scenarioWord;
        var featureData = splittedData.reduce(function (prev, item, index) {
          var codePattern = /^S(\d){3}$/g;

          if (codePattern.test(item)) {
            scenarioWord = splittedData[index + 1];
          }

          if (index + 1 > startAtIndex) {
            var _codePattern = /^BANK_(.*)_(.*)_(.*)_(.*)/g;
            var given;
            var when;
            var then;
            var s;
            var sRow;
            var tag;

            if (_codePattern.test(item)) {
              currentIndex = index;
              f = item.replace(_codePattern, "$1");
              u = item.replace(_codePattern, "$2");
              s = item.replace(_codePattern, "$3");
              sRow = item.replace(_codePattern, "$4");
              tag = "".concat(f).concat(u).concat(s).concat(sRow);
            }

            if (currentIndex > 0) {
              var convertedNewLineGiven = splittedData[currentIndex + 2].replace(/\n|\n\r|\r/g, '');
              given = convertedNewLineGiven.replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim();
              var dataSet = splittedData[currentIndex + 3]; // SplitKey

              var splittedDataset = dataSet.match(/([a-zA-Z0-9]+)(?:\s+)?=(?:\s+)?("[a-zA-Z0-9,\s\(\):-]+"|[a-zA-Z0-9.@+,-]+)/g);
              var exampleData = [];

              if (splittedDataset) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                  for (var _iterator = splittedDataset[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var data = _step.value;
                    var dataPattern = /(.*)(?:\s+)?=(?:\s+)?(.+)/g;
                    exampleData = [].concat(_toConsumableArray(exampleData), [_defineProperty({}, data.replace(dataPattern, '$1'), data.replace(dataPattern, '$2'))]);
                  }
                } catch (err) {
                  _didIteratorError = true;
                  _iteratorError = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion && _iterator.return != null) {
                      _iterator.return();
                    }
                  } finally {
                    if (_didIteratorError) {
                      throw _iteratorError;
                    }
                  }
                }
              }

              var convertedNewLineWhen = splittedData[currentIndex + 4].replace(/\n|\n\r|\r/g, '-');
              when = convertedNewLineWhen.replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim();
              var convertedNewLineThen = splittedData[currentIndex + 5].replace(/\n|\n\r|\r/g, '-');
              var showDataOnThen = [];
              var splittedThen = convertedNewLineThen.split(/\[Show\]/g);
              then = splittedThen[0].replace(/'(?!s[a-z])|"|\s(?=\s)/g, '').trim();

              if (splittedThen.length > 1) {
                showDataOnThen = splittedThen[1];
              }

              var and = then.split(/•/g).map(function (item) {
                return item.trim();
              }).filter(function (_, index) {
                return index > 1;
              });
              then = then.split(/•/g).map(function (item) {
                return item.trim();
              }).filter(function (_, index) {
                return index > 0;
              })[0] || then;
              currentIndex = 0;
              return [].concat(_toConsumableArray(prev), [{
                feature: f,
                userStory: u,
                scenario: s,
                scenarioIndex: sRow,
                given: given,
                when: when,
                then: then,
                and: and,
                exampleData: exampleData,
                showDataOnThen: showDataOnThen,
                scenarioWord: scenarioWord
              }]);
            }
          }

          return prev;
        }, []);
        resolve(featureData);
      } else {
        reject('Error', err);
        console.error('Something went wrong!!', err);
      }
    });
  });
};

var createFeatureFromTemplate =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(moduleName, featureData) {
    var baseFeature, baseUserStory, featureString;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            featureString = featureData.reduce(function (prev, current) {
              var feature = current.feature,
                  userStory = current.userStory,
                  scenario = current.scenario,
                  scenarioIndex = current.scenarioIndex,
                  showDataOnThen = current.showDataOnThen,
                  given = current.given,
                  when = current.when,
                  then = current.then,
                  and = current.and,
                  givenAnd = current.givenAnd,
                  scenarioWord = current.scenarioWord,
                  exampleData = current.exampleData;

              if (!baseFeature) {
                baseFeature = feature;
              }

              if (!baseUserStory) {
                baseUserStory = userStory;
              }

              var expectedShowData = showDataOnThen.length > 0 ? showDataOnThen.split('•').filter(function (item) {
                return item.trim() !== '';
              }).map(function (item) {
                return item.trim();
              }).map(function (item) {
                var dataPattern = /(.*)\s*=\s*(.*)/g;
                var key = (0, _capitalize2.default)(item.replace(dataPattern, '$1').trim().replace(/\s/g, '_'));
                var value = item.replace(dataPattern, '$2');
                return _defineProperty({}, key, value);
              }) : [];

              var generateTable = function generateTable(datas) {
                var header = datas.reduce(function (prev, current, _) {
                  var key = Object.keys(current)[0];
                  return "".concat(prev, " ").concat(key, " |");
                }, '|');
                var dataItem = datas.reduce(function (prev, current, _) {
                  var value = Object.values(current)[0];
                  return "".concat(prev, " ").concat(value, " |");
                }, '|');
                return "".concat(header, "\n        ").concat(dataItem);
              };

              var trutlyThen = "".concat(then).concat(showDataOnThen.length > 0 ? "\n".concat(generateTable(expectedShowData)) : '');

              if (and.length > 0) {
                var combinedAnd = and.reduce(function (prev, current) {
                  return "".concat(prev, "\n    And ").concat(current);
                }, '');
                trutlyThen = "".concat(then).concat(showDataOnThen.length > 0 ? "\n        ".concat(generateTable(expectedShowData)) : '').concat(combinedAnd);
              }

              var trutlyGiven = given;

              if (givenAnd.length > 0) {
                var _combinedAnd = givenAnd.reduce(function (prev, current) {
                  return "".concat(prev, "\n    And ").concat(current);
                }, '');

                trutlyGiven = "".concat(given).concat(exampleData.length > 0 ? "\n        ".concat(generateTable(exampleData)) : '').concat(_combinedAnd);
              }

              var tag = "".concat(feature).concat(userStory).concat(scenario).concat(scenarioIndex);
              var replacedTemplate = scenarioTemplate.replace(/__%TAG%__/g, tag).replace(/__%MODULE_NAME%__/g, moduleName).replace(/__%FEATURE_NAME%__/g, moduleName).replace(/__%SCENARIO_NUMBER%__/g, "".concat(scenario, "_").concat(scenarioIndex)).replace(/__%SCENARIO_WORD%__/g, scenarioWord).replace(/__%GIVEN%__/g, "".concat(trutlyGiven.trim())).replace(/__%WHEN%__/g, when.trim()).replace(/__%THEN%__/g, trutlyThen.trim());
              return prev + replacedTemplate;
            }, '');
            createFeatureFile(moduleName, baseFeature, baseUserStory, scenarioHeaderTemplate + featureString);

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function createFeatureFromTemplate(_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}();

var createCommonFromTemplate =
/*#__PURE__*/
function () {
  var _ref4 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(moduleName, dupplicatedData) {
    var replacedDataWhen, replacedDataGiven, replacedDataThen, replacedDataAnd, replacedDataGivenAnd;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            replacedDataWhen = dupplicatedData.when.reduce(function (prev, item) {
              return prev + commonStepTemplate.when.replace('__%WHEN%__', item);
            }, '');
            replacedDataGiven = dupplicatedData.given.reduce(function (prev, item) {
              return prev + commonStepTemplate.given.replace('__%GIVEN%__', item);
            }, '');
            replacedDataThen = dupplicatedData.then.reduce(function (prev, item) {
              return prev + commonStepTemplate.then.replace('__%THEN%__', item);
            }, '');
            replacedDataAnd = dupplicatedData.and.reduce(function (prev, item) {
              return prev + commonStepTemplate.then.replace('__%THEN%__', item.replace(/\//g, '\\/').replace(/\(/g, '\\(').replace(/\)/g, '\\)'));
            }, '');
            replacedDataGivenAnd = dupplicatedData.givenAnd.reduce(function (prev, item) {
              return prev + commonStepTemplate.then.replace('__%THEN%__', item.replace(/\//g, '\\/').replace(/\(/g, '\\(').replace(/\)/g, '\\)'));
            }, '');
            createCommonFile(moduleName, "import { Given, When, Then } from 'cucumber'\n\n" + '\n// Auto-generate Given \n\n' + replacedDataGiven + '\n// Auto-generate Given-And \n\n' + replacedDataGivenAnd + '\n// Auto-generate When \n\n' + replacedDataWhen + '\n // Auto-generate Then \n\n' + replacedDataThen + '\n// Auto-generate And \n\n' + replacedDataAnd);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function createCommonFromTemplate(_x3, _x4) {
    return _ref4.apply(this, arguments);
  };
}();

var getComplatibleQuote = function getComplatibleQuote(data) {
  var singleQuotePattern = /'/g;
  var doubleQuotePattern = /"/g;
  var foundSingleQuote = singleQuotePattern.test(data);
  var foundDoubleQuote = doubleQuotePattern.test(data);

  if (foundDoubleQuote && foundSingleQuote) {
    return "`";
  } else if (foundSingleQuote) {
    return "\"";
  }

  return "'";
};

var createScenarioStepFromTemplate =
/*#__PURE__*/
function () {
  var _ref5 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(moduleName, data, dupplicatedData) {
    var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _loop2, _iterator2, _step2;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _iteratorNormalCompletion2 = true;
            _didIteratorError2 = false;
            _iteratorError2 = undefined;
            _context3.prev = 3;

            _loop2 = function _loop2() {
              var scenarioItem = _step2.value;
              var extendedGiven = "".concat(scenarioItem.given);
              var replaceEscapeCharacter = {
                given: extendedGiven.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
                when: scenarioItem.when.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
                then: scenarioItem.then.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)')
              };
              var replacedData = scenarioStepTemplate.replace("__%GIVEN%__", "".concat(replaceEscapeCharacter.given)).replace("__%WHEN%__", "".concat(getComplatibleQuote(replaceEscapeCharacter.when)).concat(replaceEscapeCharacter.when).concat(getComplatibleQuote(replaceEscapeCharacter.when))).replace("__%THEN%__", "".concat(getComplatibleQuote(replaceEscapeCharacter.then)).concat(replaceEscapeCharacter.then).concat(getComplatibleQuote(replaceEscapeCharacter.then))).replace(/__%SLASH%__/g, "\\/");
              var removedDupplicateWhen = dupplicatedData.when.includes(scenarioItem.when) ? replacedData.replace(/When\((.*)}\)/g, "// ! WHEN - \"".concat(scenarioItem.when, "\" IS DUPLICATED!! It will be at common step.")) : replacedData;
              var removedDupplicateGiven = dupplicatedData.given.includes(scenarioItem.given) ? removedDupplicateWhen.replace(/Given\((.*)\n.*\n.*\n.*\n.*\n.*\n.*\n}\)/g, "// ! GIVEN - \"".concat(scenarioItem.given, "\" IS DUPLICATED!! It will be at common step.")) : removedDupplicateWhen;
              var removedDupplicateThen = dupplicatedData.then.includes(scenarioItem.then) ? removedDupplicateGiven.replace(/Then\((.*)}\)/g, "// ! THEN - \"".concat(scenarioItem.then, "\" IS DUPLICATED!! It will be at common step.")) : removedDupplicateGiven; // Then And Logic

              var trutlyDupllicated = '';

              if (dupplicatedData.and.length > 0) {
                var escapedCharAdd = scenarioItem.and.map(function (addItem) {
                  return addItem.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)');
                });
                trutlyDupllicated = escapedCharAdd.reduce(function (prev, current, index) {
                  var thenStatement = function thenStatement(sentense) {
                    return "\n\nThen(".concat(getComplatibleQuote(sentense)).concat(sentense).concat(getComplatibleQuote(sentense), ", async () => { /* TODO: ADD STEP HERE */ })");
                  };

                  var removeDuplicateAnd = dupplicatedData.and.includes(scenarioItem.and[index]) ? "\n\n// ! AND - \"".concat(current, "\" IS DUPLICATED!! It will be at common step.") : thenStatement(current).replace(/__%SLASH%__/g, "\\/");
                  return "".concat(prev).concat(removeDuplicateAnd);
                }, '');
              }

              var formattedData = "".concat(removedDupplicateThen).concat(trutlyDupllicated).replace(/(async \(\) => {)(.*)(}\))/g, '$1\n$2\n$3'); // Given And Logic

              var trutlyDupllicatedGivenAnd = '';

              if (dupplicatedData.givenAnd.length > 0) {
                var _escapedCharAdd = scenarioItem.givenAnd.map(function (addItem) {
                  return addItem.replace(/\//g, '__%SLASH%__').replace(/\(/g, '\\(').replace(/\)/g, '\\)');
                });

                trutlyDupllicatedGivenAnd = _escapedCharAdd.reduce(function (prev, current, index) {
                  var thenStatement = function thenStatement(sentense) {
                    return "\n\nThen(".concat(getComplatibleQuote(sentense)).concat(sentense).concat(getComplatibleQuote(sentense), ", async () => { /* TODO: ADD STEP HERE */ })");
                  };

                  var removeDuplicateAnd = dupplicatedData.givenAnd.includes(scenarioItem.givenAnd[index]) ? "\n\n// ! Given-And - \"".concat(current, "\" IS DUPLICATED!! It will be at common step.") : thenStatement(current).replace(/__%SLASH%__/g, "\\/");
                  return "".concat(prev).concat(removeDuplicateAnd);
                }, '');
              }

              var formattedData2 = formattedData.replace(/__%GIVEN_AND%__/, trutlyDupllicatedGivenAnd).replace(/(async \(\) => {)(.*)(}\))/g, '$1\n$2\n$3');
              createScenarioStepFile(moduleName, scenarioItem.scenario, scenarioItem.scenarioIndex, formattedData2);
            };

            for (_iterator2 = data[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              _loop2();
            }

            _context3.next = 12;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](3);
            _didIteratorError2 = true;
            _iteratorError2 = _context3.t0;

          case 12:
            _context3.prev = 12;
            _context3.prev = 13;

            if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
              _iterator2.return();
            }

          case 15:
            _context3.prev = 15;

            if (!_didIteratorError2) {
              _context3.next = 18;
              break;
            }

            throw _iteratorError2;

          case 18:
            return _context3.finish(15);

          case 19:
            return _context3.finish(12);

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[3, 8, 12, 20], [13,, 15, 19]]);
  }));

  return function createScenarioStepFromTemplate(_x5, _x6, _x7) {
    return _ref5.apply(this, arguments);
  };
}();

var validatingDirectory = function validatingDirectory(moduleName) {
  return new Promise(function (resolve, reject) {
    try {
      if (!_fs.default.existsSync(generatingDir)) {
        _fs.default.mkdirSync(generatingDir);

        automateProcessLog('Created Directory >> ' + generatingDir);
      }

      if (!_fs.default.existsSync(featureDir)) {
        _fs.default.mkdirSync(featureDir);

        automateProcessLog('Created Directory >> ' + featureDir);
      }

      if (!_fs.default.existsSync(BDDFeature)) {
        _fs.default.mkdirSync(BDDFeature);

        automateProcessLog('Created Directory >> ' + BDDFeature);
      }

      if (!_fs.default.existsSync(stepDir)) {
        _fs.default.mkdirSync(stepDir);

        automateProcessLog('Created Directory >> ' + stepDir);
      }

      if (!_fs.default.existsSync(commonStepDir)) {
        _fs.default.mkdirSync(commonStepDir);

        automateProcessLog('Created Directory >> ' + commonStepDir);
      }

      if (!_fs.default.existsSync(scenarioStepDir)) {
        _fs.default.mkdirSync(scenarioStepDir);

        automateProcessLog('Created Directory >> ' + scenarioStepDir);
      }

      var moduleFolder = "".concat(BDDFeature, "/").concat(moduleName);
      var moduleStepFolder = "".concat(scenarioStepDir, "/").concat(moduleName);

      if (!_fs.default.existsSync(moduleFolder)) {
        _fs.default.mkdirSync(moduleFolder);

        automateProcessLog('Created Directory >> ' + moduleFolder);
      }

      if (!_fs.default.existsSync(moduleStepFolder)) {
        _fs.default.mkdirSync(moduleStepFolder);

        automateProcessLog('Created Directory >> ' + moduleStepFolder);
      }

      resolve();
    } catch (error) {
      reject(error);
    }
  });
};

var runJob =
/*#__PURE__*/
function () {
  var _ref6 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(moduleName, path) {
    var rawData, data, dupplicatedGiven, givenAndCase, dupplicatedGivenAnd, dupplicatedWhen, dupplicatedThen, andCase, dupplicatedAnd;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return validatingDirectory(moduleName);

          case 3:
            _context4.next = 5;
            return extractDataFromSourceFile(path);

          case 5:
            rawData = _context4.sent;
            automateProcessLog(" Total Scenario: \t" + chalk.green(rawData.length));
            data = rawData.map(function (dataItem) {
              var splittedGiven = dataItem.given.split(/and/);
              var given = splittedGiven[0].trim();
              var givenAnd = splittedGiven.filter(function (_, index) {
                return index > 0;
              }).map(function (item) {
                return item.trim();
              });
              return _objectSpread({}, dataItem, {
                given: given,
                givenAnd: givenAnd
              });
            });
            dupplicatedGiven = findDuplicatedData(data, 'given');
            console.log(chalk.bold("\n  \uD83C\uDF08  Duplicate Given: \t\t".concat(dupplicatedGiven.length)));
            givenAndCase = data.reduce(function (prev, current) {
              return [].concat(_toConsumableArray(prev), _toConsumableArray(current.givenAnd));
            }, []).reduce(function (prev, current) {
              return prev.includes(current) ? prev : [].concat(_toConsumableArray(prev), [{
                givenAnd: current
              }]);
            }, []);
            dupplicatedGivenAnd = findDuplicatedData(givenAndCase, 'givenAnd').filter(function (item) {
              return item !== '';
            });
            console.log(chalk.bold("  \uD83C\uDF08  Duplicate Given-And: \t".concat(dupplicatedGivenAnd.length)));
            dupplicatedWhen = findDuplicatedData(data, 'when');
            console.log(chalk.bold("  \uD83C\uDF08  Duplicate When: \t\t".concat(dupplicatedWhen.length)));
            dupplicatedThen = findDuplicatedData(data, 'then');
            console.log(chalk.bold("  \uD83C\uDF08  Duplicate Then: \t\t".concat(dupplicatedThen.length)));
            andCase = data.reduce(function (prev, current) {
              return [].concat(_toConsumableArray(prev), _toConsumableArray(current.and));
            }, []).reduce(function (prev, current) {
              return prev.includes(current) ? prev : [].concat(_toConsumableArray(prev), [{
                and: current
              }]);
            }, []);
            dupplicatedAnd = findDuplicatedData(andCase, 'and');
            console.log(chalk.bold("  \uD83C\uDF08  Duplicate ThenAnd: \t".concat(dupplicatedAnd.length, "\n")));
            _context4.next = 22;
            return createFeatureFromTemplate(moduleName, data);

          case 22:
            _context4.next = 24;
            return createCommonFromTemplate(moduleName, {
              given: dupplicatedGiven,
              givenAnd: dupplicatedGivenAnd,
              then: dupplicatedThen,
              when: dupplicatedWhen,
              and: dupplicatedAnd
            });

          case 24:
            _context4.next = 26;
            return createScenarioStepFromTemplate(moduleName, data, {
              given: dupplicatedGiven,
              givenAnd: dupplicatedGivenAnd,
              then: dupplicatedThen,
              when: dupplicatedWhen,
              and: dupplicatedAnd
            });

          case 26:
            console.log(chalk.bold("\n\u2728 Success \u2728\n"));
            _context4.next = 32;
            break;

          case 29:
            _context4.prev = 29;
            _context4.t0 = _context4["catch"](0);
            console.error(_context4.t0);

          case 32:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 29]]);
  }));

  return function runJob(_x8, _x9) {
    return _ref6.apply(this, arguments);
  };
}();

module.exports = runJob;