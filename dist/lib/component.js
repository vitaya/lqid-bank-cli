"use strict";

var fs = require('fs');

var chalk = require('chalk');

var glob = require('glob');

var statelessComponentTemplate = require('../template/components/stlComponent.template');

var statefulComponentTemplate = require('../template/components/stfComponent.template');

var unitTestTemplate = require('../template/components/unit-test.template');

var styleTemplate = require('../template/components/style.template');

var componentGeneratingProcessLog = function componentGeneratingProcessLog(word) {
  console.log('  ' + chalk.bold("\uD83C\uDF54  ".concat(word)));
};

var convertTocamelCase = function convertTocamelCase(string) {
  var firstCharPattern = /(\w)(\w+)/g;
  var firstString = string.replace(firstCharPattern, "$1").toLowerCase();
  var otherString = string.replace(firstCharPattern, "$2");
  return "".concat(firstString).concat(otherString);
};

var createFile = function createFile(path, data) {
  fs.writeFile(path, data, function (err) {
    if (err) {
      return console.log(err);
    }

    componentGeneratingProcessLog(chalk.red('File: ') + path);
  });
};

var importExportTemplateGen = function importExportTemplateGen(moduleName, path) {
  return "import ".concat(moduleName, " from '").concat(path, "'\n\nexport { ").concat(moduleName, " } \n  ");
};

var createDir = function createDir(dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    componentGeneratingProcessLog(chalk.green('Directory: ') + dir);
  }
};

var generateComponent = function generateComponent(moduleName, isStateless, path) {
  var createPath = path || '';
  var unitTestDir = "".concat(moduleName, "/__tests__");
  var stylesDir = "".concat(moduleName, "/stylesheets");
  var moduleNamePath = "".concat(createPath).concat(moduleName);
  var unitTestDirPath = "".concat(createPath).concat(unitTestDir);
  var stylesDirPath = "".concat(createPath).concat(stylesDir);
  var importStatement = "import { ".concat(moduleName, " } from './").concat(moduleName, "'\n$1");
  var exportStatement = "".concat(moduleName, ",\n  $1");
  updateIndexFile(createPath, /(\/\/! auto-generator-import)/g, importStatement, function () {
    updateIndexFile(createPath, /(\/\/! auto-generator-export)/g, exportStatement, function () {
      componentGeneratingProcessLog(chalk.blue('Modify: index.js'));
    });
  });
  createDir(moduleNamePath);
  createDir(unitTestDirPath);
  createDir(stylesDirPath);
  var patternReplaceStyle = new RegExp("".concat(moduleName, "Style"), 'g'); //   // Create Component File

  var addedModuleNameData = isStateless ? statelessComponentTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName).replace(patternReplaceStyle, "".concat(convertTocamelCase(moduleName), "Style")) : statefulComponentTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName).replace(patternReplaceStyle, "".concat(convertTocamelCase(moduleName), "Style"));
  createFile("".concat(moduleNamePath, "/").concat(moduleName, ".js"), addedModuleNameData);
  createFile("".concat(moduleNamePath, "/index.js"), importExportTemplateGen(moduleName, "./".concat(moduleName))); // Create Unit-test Files

  var addedModuleNameUnitTestData = unitTestTemplate.replace(/__%COMPONENT_NAME%__/g, moduleName);
  createFile("".concat(unitTestDirPath, "/").concat(moduleName, ".spec.js"), addedModuleNameUnitTestData); // Create style Files

  var addedModuleNameStyleData = styleTemplate;
  createFile("".concat(stylesDirPath, "/").concat(convertTocamelCase(moduleName), ".style.js"), addedModuleNameStyleData);
  createFile("".concat(stylesDirPath, "/index.js"), importExportTemplateGen(convertTocamelCase(moduleName) + 'Style', "./".concat(convertTocamelCase(moduleName), ".style")));
};

var updateIndexFile = function updateIndexFile(path, aboveOfPattern, contentToAdd, cb) {
  glob("".concat(path, "index.js"), null, function (err, files) {
    var indexFile = files.filter(function (fileName) {
      return /^.*index.js$/.test(fileName);
    });

    if (indexFile.length > 0) {
      fs.readFile(indexFile[0], function (err, data) {
        var indexFilesContent = data.toString();
        var addedContentData = indexFilesContent.replace(aboveOfPattern, "".concat(contentToAdd));
        fs.writeFile(indexFile[0], addedContentData, function (err) {
          if (err) {
            return console.log(err);
          }

          cb();
        });
      });
    } else {
      componentGeneratingProcessLog(chalk.red('Cant Modify: index.js [Not found "index.js"]'));
    }
  });
};

var componentGenerator = function componentGenerator(moduleName, isStateless, path) {
  generateComponent(moduleName, isStateless, path);
};

module.exports = componentGenerator;