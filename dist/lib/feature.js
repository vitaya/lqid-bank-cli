"use strict";

var chalk = require('chalk');

var fileUtils = require('../utils/file');

var constantTemplate = require('../template/features/constant.template');

var utilsTemplate = require('../template/features/utils.template');

var transformTemplate = require('../template/features/transform.template');

var indexTemplate = require('../template/features/index.template');

var reducerTemplate = require('../template/features/reducer.template');

var routeTemplate = require('../template/features/routeIndex.template');

var featureIndexTemplate = require('../template/features/featureIndex.template');

var logFileTree = function logFileTree(module_name, path) {
  console.log(chalk.bold.red("======================="));
  console.log(chalk.bold.red("+✨ GENERATED FILES ✨+"));
  console.log(chalk.bold.red("======================="));
  console.log(chalk.red("\n".concat(path).concat(module_name, "\n   |- assets\n   |   |- images\n   |     |- .gitkeep\n   |- commons\n   |   |- constants.js\n   |   |- utils.js\n   |   |- transforms.js\n   |- components\n   |   |- index.js\n   |- containers\n   |   |- index.js\n   |- models\n   |   |- index.js\n   |- redux\n   |   |- reducers.js\n   |- routes\n   |   |- index.js\n   |- scenes\n   |   |- index.js\n   |- services\n   |   |- endpoints.js\n   |   |- index.js\n   |- index.js")));
  console.log(chalk.bold.red("\n=======================\n"));
};

var generateFiles = function generateFiles(moduleName) {
  var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  // Directory
  var baseDir = "".concat(path).concat(moduleName);
  var assetsDir = "".concat(baseDir, "/assets");
  var imagesDir = "".concat(assetsDir, "/images");
  var commonsDir = "".concat(baseDir, "/commons");
  var componentsDir = "".concat(baseDir, "/components");
  var containersDir = "".concat(baseDir, "/containers");
  var modelsDir = "".concat(baseDir, "/models");
  var reduxDir = "".concat(baseDir, "/redux");
  var routesDir = "".concat(baseDir, "/routes");
  var scenesDir = "".concat(baseDir, "/scenes");
  var servicesDir = "".concat(baseDir, "/services"); // Files

  var featureIndex = "".concat(baseDir, "/index.js");
  var common = {
    constant: "".concat(commonsDir, "/constants.js"),
    utils: "".concat(commonsDir, "/utils.js"),
    transform: "".concat(commonsDir, "/transforms.js")
  };
  var component = {
    index: "".concat(componentsDir, "/index.js")
  };
  var container = {
    index: "".concat(containersDir, "/index.js")
  };
  var model = {
    index: "".concat(modelsDir, "/index.js")
  };
  var scene = {
    index: "".concat(scenesDir, "/index.js")
  };
  var reducer = {
    index: "".concat(reduxDir, "/reducers.js")
  };
  var routes = {
    index: "".concat(routesDir, "/index.js")
  };
  var services = {
    index: "".concat(servicesDir, "/index.js")
  };
  fileUtils.createDir(baseDir);
  fileUtils.createFile(featureIndex, featureIndexTemplate(moduleName));
  fileUtils.createDir(assetsDir);
  fileUtils.createDir(imagesDir);
  fileUtils.createDir(commonsDir); // Create Common Files

  fileUtils.createFile(common.constant, constantTemplate);
  fileUtils.createFile(common.utils, utilsTemplate);
  fileUtils.createFile(common.transform, transformTemplate);
  fileUtils.createDir(componentsDir); // Create Component Files

  fileUtils.createFile(component.index, indexTemplate);
  fileUtils.createDir(containersDir); // Create Container Files

  fileUtils.createFile(container.index, indexTemplate);
  fileUtils.createDir(modelsDir); // Create Model Files

  fileUtils.createFile(model.index, indexTemplate);
  fileUtils.createDir(reduxDir); // Create Reducer Files

  fileUtils.createFile(reducer.index, reducerTemplate(moduleName));
  fileUtils.createDir(routesDir); // Create Router Files

  fileUtils.createFile(routes.index, routeTemplate(moduleName));
  fileUtils.createDir(scenesDir); // Create Scene Files

  fileUtils.createFile(scene.index, indexTemplate);
  fileUtils.createDir(servicesDir); // Create Service Files

  fileUtils.createFile(services.index, indexTemplate);
};

var featureGenerator = function featureGenerator(module_name, path) {
  logFileTree(module_name, path);
  generateFiles(module_name, path);
};

module.exports = featureGenerator;