"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var program = require('commander');

var chalk = require('chalk');

var _require = require('lodash'),
    upperFirst = _require.upperFirst;

var fs = require('fs');

var shell = require('child_process').execSync;

var inquirer = require('inquirer');

var _require2 = require('inquirer-path'),
    PathPrompt = _require2.PathPrompt;

var _require3 = require('../utils/file'),
    createFile = _require3.createFile,
    createDir = _require3.createDir;

inquirer.registerPrompt('path', PathPrompt); // redux

var indexTemplate = require('../template/reducer/index.template');

var actionTypesTemplate = require('../template/reducer/actionTypes.template');

var actionCreatorsTemplate = require('../template/reducer/actionCreators.template');

var reducersTemplate = require('../template/reducer/reducers.template');

var initialStateTemplate = require('../template/reducer/initialState.template');

var selectorsTemplate = require('../template/reducer/selectors.template'); // unit test redux


var unitTestReducersTemplate = require('../template/reducer/__tests__/reducers.spec.template');

var unitTestActionCreatorsTemplate = require('../template/reducer/__tests__/actionCreators.spec.template');

var unitTestSelectorsTemplate = require('../template/reducer/__tests__/selectors.spec.template');
/**
 * Defined variable command
 * --
 */


var SERVICE_NAME = 'SERVICE_NAME';
var ENDPOINT = 'ENDPOINT';
var REDUCER_NAME = 'REDUCER_NAME';
var ACTION_TYPE_NAME = 'ACTION_TYPE_NAME';
var ACTION_CREATOR_NAME = 'ACTION_CREATOR_NAME';
var FEATURE_PATH = 'FEATURE_PATH';
var IMPORT_SERVICE_PATH = ''; // TODO Gen. Service function

function replaceWording(pathFile, fileContent) {
  var wording = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  // Replace wording in file
  Object.entries(wording).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];

    var pattern = new RegExp(key, 'g');
    fileContent = fileContent.replace(pattern, value);
  }); // Write file with include new wording

  fs.writeFileSync(pathFile, fileContent, {
    encoding: 'utf8'
  });
} // TODO Gen. Service
// TODO Plugin reducers
// TODO log task
// TODO with Sub reducer ?
// TODO Selector name


function execute() {
  inquirer.prompt([{
    type: 'input',
    name: SERVICE_NAME,
    message: 'Enter service name?',
    suffix: ' (ex. getAccountDetail) :'
  }, // { type: 'input', name: ENDPOINT,            message: 'Enter your endpoint name?' },
  {
    type: 'input',
    name: REDUCER_NAME,
    message: 'Enter reducer name?',
    suffix: ' (ex. accountDetail) :'
  }, {
    type: 'input',
    name: ACTION_CREATOR_NAME,
    message: 'Enter actionCreator name?',
    suffix: ' (ex. getAccountDetail) :'
  }, {
    type: 'input',
    name: ACTION_TYPE_NAME,
    message: 'Enter actionType name?',
    suffix: ' (ex. GET_ACCOUNT_DETAIL) :'
  }, {
    type: 'path',
    name: FEATURE_PATH,
    message: 'Enter feature path',
    suffix: ' (optional) :'
  }]).then(function (answers) {
    var rootReduxPath = "".concat(answers[FEATURE_PATH], "/redux");
    var reduxPath = "".concat(rootReduxPath, "/").concat(answers[REDUCER_NAME]); // Path file

    var indexPath = "".concat(reduxPath, "/index.js");
    var actionCreatorsPath = "".concat(reduxPath, "/actionCreators.js");
    var actionTypesPath = "".concat(reduxPath, "/actionTypes.js");
    var reducersPath = "".concat(reduxPath, "/reducers.js");
    var selectorsPath = "".concat(reduxPath, "/selectors.js");
    var initialStatePath = "".concat(reduxPath, "/initialState.js");
    IMPORT_SERVICE_PATH = "".concat(answers[FEATURE_PATH], "/services"); // Unit test

    var unitTestActionCreatorsPath = "".concat(reduxPath, "/__tests__/actionCreators.spec.js");
    var unitTestReducerPath = "".concat(reduxPath, "/__tests__/reducers.spec.js");
    var unitTestSelectorPath = "".concat(reduxPath, "/__tests__/selectors.spec.js"); // Create dir

    createDir(rootReduxPath);
    createDir(reduxPath);
    createDir("".concat(reduxPath, "/__tests__")); // index

    replaceWording(indexPath, indexTemplate, _defineProperty({
      ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME],
      REDUCER_NAME: answers[REDUCER_NAME]
    }, "select".concat(answers[REDUCER_NAME]), "select".concat(upperFirst(answers[REDUCER_NAME])))); // initialState

    createFile(initialStatePath, initialStateTemplate); // actionTypes

    replaceWording(actionTypesPath, actionTypesTemplate, {
      ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME]
    }); // reducers

    replaceWording(reducersPath, reducersTemplate, {
      ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
      REDUCER_NAME: answers[REDUCER_NAME]
    }); // [Unit test] reducers

    replaceWording(unitTestReducerPath, unitTestReducersTemplate, {
      SERVICE_NAME: answers[SERVICE_NAME],
      ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
      REDUCER_NAME: upperFirst(answers[REDUCER_NAME])
    }); // actionCreators

    replaceWording(actionCreatorsPath, actionCreatorsTemplate, {
      SERVICE_NAME: answers[SERVICE_NAME],
      // IMPORT_SERVICE_PATH: IMPORT_SERVICE_PATH,
      ACTION_TYPE_NAME: answers[ACTION_TYPE_NAME],
      ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME]
    }); // [Unit test] actionCreators

    replaceWording(unitTestActionCreatorsPath, unitTestActionCreatorsTemplate, {
      SERVICE_NAME: answers[SERVICE_NAME],
      REDUCER_NAME: upperFirst(answers[REDUCER_NAME]),
      // IMPORT_SERVICE_PATH: IMPORT_SERVICE_PATH,
      ACTION_CREATOR_NAME: answers[ACTION_CREATOR_NAME]
    }); // selectorsPath

    replaceWording(selectorsPath, selectorsTemplate, _defineProperty({
      REDUCER_NAME: answers[REDUCER_NAME]
    }, "select".concat(answers[REDUCER_NAME]), "select".concat(upperFirst(answers[REDUCER_NAME])))); // [Unit test] selectorsPath

    replaceWording(unitTestSelectorPath, unitTestSelectorsTemplate, _defineProperty({
      REDUCER_NAME: answers[REDUCER_NAME]
    }, "select".concat(answers[REDUCER_NAME]), "select".concat(upperFirst(answers[REDUCER_NAME]))));
  });
}

module.exports = execute;