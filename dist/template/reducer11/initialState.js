"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialState = void 0;
var initialState = {
  isFetching: false,
  data: {},
  error: {},
  code: null
};
exports.initialState = initialState;