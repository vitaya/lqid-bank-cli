"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectREDUCER_NAME = void 0;

var _get2 = _interopRequireDefault(require("lodash/get"));

var _Configs = require("@Configs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var selectREDUCER_NAME = function selectREDUCER_NAME(state, name) {
  return (0, _get2.default)(state[_Configs.project.name], "REDUCER_NAME.".concat(name), state[_Configs.project.name].REDUCER_NAME);
};

exports.selectREDUCER_NAME = selectREDUCER_NAME;