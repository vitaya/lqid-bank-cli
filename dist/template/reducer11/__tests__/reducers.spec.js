"use strict";

var _Utils = require("@Utils");

var _Constants = require("@Constants");

var _reducers = _interopRequireDefault(require("../reducers"));

var _initialState = require("../initialState");

var _actionTypes = require("../actionTypes");

var _SERVICE_NAME = require("../../../services/SERVICE_NAME");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

describe('REDUCER_NAME reducer', function () {
  it('should return initial state', function () {
    expect((0, _reducers.default)(undefined, {})).toEqual(_initialState.initialState);
  });
  it('should not handle when action type is not match handle case', function () {
    var action = {
      type: 'SOME_ACTION_TYPE'
    };
    var actualState = undefined;
    var expectState = _initialState.initialState;
    expect((0, _reducers.default)(actualState, action)).toEqual(expectState);
  });
  it("should handle case ".concat(_actionTypes.ACTION_TYPE_NAME.REQUEST), function () {
    var action = {
      type: _actionTypes.ACTION_TYPE_NAME.REQUEST
    };
    var actualState = undefined;

    var expectState = _objectSpread({}, _initialState.initialState, {
      isFetching: true
    });

    expect((0, _reducers.default)(actualState, action)).toEqual(expectState);
  });
  it("should handle case ".concat(_actionTypes.ACTION_TYPE_NAME.SUCCESS), function () {
    var action = {
      type: _actionTypes.ACTION_TYPE_NAME.SUCCESS,
      code: _SERVICE_NAME.mock.success.data.code,
      data: _SERVICE_NAME.mock.success.data.data
    };
    var actualState = _initialState.initialState;

    var expectState = _objectSpread({}, _initialState.initialState, {
      isFetching: _initialState.initialState.isFetching,
      code: _SERVICE_NAME.mock.success.data.code,
      data: _SERVICE_NAME.mock.success.data.data
    });

    expect((0, _reducers.default)(actualState, action)).toEqual(expectState);
  });
  it("should handle case ".concat(_actionTypes.ACTION_TYPE_NAME.FAILURE), function () {
    var _handleErrorResponse = (0, _Utils.handleErrorResponse)(_SERVICE_NAME.mock.failure),
        error = _handleErrorResponse.error,
        code = _handleErrorResponse.code;

    var action = {
      type: _actionTypes.ACTION_TYPE_NAME.FAILURE,
      error: error,
      code: code
    };
    var actualState = _initialState.initialState;

    var expectState = _objectSpread({}, _initialState.initialState, {
      isFetching: _initialState.initialState.isFetching,
      error: error,
      code: code
    });

    expect((0, _reducers.default)(actualState, action)).toEqual(expectState);
  });
  it("should clear all state when receive action type \"".concat(_Constants.LOG_OUT, "\""), function () {
    var action = {
      type: _Constants.LOG_OUT
    };

    var actualState = _objectSpread({}, _initialState.initialState, {
      code: _SERVICE_NAME.mock.success.data.code,
      data: _SERVICE_NAME.mock.success.data.data
    });

    var expectState = _initialState.initialState;
    expect((0, _reducers.default)(actualState, action)).toEqual(expectState);
  });
});