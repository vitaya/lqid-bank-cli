"use strict";

var _Configs = require("@Configs");

var _selectors = require("../selectors");

var _initialState = require("../initialState");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = _defineProperty({}, _Configs.project.name, {
  REDUCER_NAME: _initialState.initialState
});

describe('Selectors', function () {
  describe('selectREDUCER_NAME', function () {
    it('should be return an object of store, specific by name', function () {
      expect((0, _selectors.selectREDUCER_NAME)(state)).toEqual(_initialState.initialState);
      expect((0, _selectors.selectREDUCER_NAME)(state, 'isFetching')).toEqual(_initialState.initialState.isFetching);
      expect((0, _selectors.selectREDUCER_NAME)(state, 'data')).toEqual(_initialState.initialState.data);
      expect((0, _selectors.selectREDUCER_NAME)(state, 'error')).toEqual(_initialState.initialState.error);
      expect((0, _selectors.selectREDUCER_NAME)(state, 'code')).toEqual(_initialState.initialState.code);
    });
  });
});