"use strict";

var _axios = _interopRequireDefault(require("../../../../../../__mocks__/axios"));

var _mockStore = _interopRequireDefault(require("../../../../../config/mockStore"));

var _actionCreators = require("./../actionCreators");

var _SERVICE_NAME = require("../../../services/SERVICE_NAME");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var store = (0, _mockStore.default)();
describe('REDUCER_NAME action creators', function () {
  beforeEach(function () {
    store.clearActions();
  });
  describe('ACTION_CREATOR_NAME action', function () {
    it('should be create an async action with handle response success',
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _axios.default.mockResponseSuccess(_SERVICE_NAME.mock.success);

              _context.next = 3;
              return store.dispatch((0, _actionCreators.ACTION_CREATOR_NAME)());

            case 3:
              expect(store.getActions()).toMatchSnapshot();

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })));
    it('should be handle and create an async action with handle response failure',
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _axios.default.mockResponseFailure(_SERVICE_NAME.mock.failure);

              _context2.next = 3;
              return store.dispatch((0, _actionCreators.ACTION_CREATOR_NAME)());

            case 3:
              expect(store.getActions()).toMatchSnapshot();

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    })));
  });
});