"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ACTION_TYPE_NAME = void 0;

var _Utils = require("@Utils");

var ACTION_TYPE_NAME = (0, _Utils.actionTypeCreator)('ACTION_TYPE_NAME');
exports.ACTION_TYPE_NAME = ACTION_TYPE_NAME;