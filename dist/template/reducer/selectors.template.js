"use strict";

module.exports = "\nimport _get from 'lodash/get'\nimport { project } from '@Configs'\n\nexport const selectREDUCER_NAME = (state, name) => _get(state[project.name], `REDUCER_NAME.${name}`, state[project.name].REDUCER_NAME)\n";