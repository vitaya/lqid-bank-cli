"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Constants = require("@Constants");

var _initialState = require("./initialState");

var _actionTypes = require("./actionTypes");

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var REDUCER_NAMEReducer = function REDUCER_NAMEReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _initialState.initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actionTypes.ACTION_TYPE_NAME.REQUEST:
      {
        return _objectSpread({}, state, {
          isFetching: true,
          code: _initialState.initialState.code
        });
      }

    case _actionTypes.ACTION_TYPE_NAME.SUCCESS:
      {
        return _objectSpread({}, state, {
          isFetching: false,
          error: _initialState.initialState.error,
          code: action.code,
          data: action.data
        });
      }

    case _actionTypes.ACTION_TYPE_NAME.FAILURE:
      {
        return _objectSpread({}, state, {
          isFetching: false,
          code: action.code,
          error: action.error,
          data: _initialState.initialState.data
        });
      }

    case _Constants.LOG_OUT:
      return _initialState.initialState;

    default:
      return state;
  }
};

var _default = REDUCER_NAMEReducer;
exports.default = _default;