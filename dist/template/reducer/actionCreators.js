"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ACTION_CREATOR_NAME = void 0;

var _get2 = _interopRequireDefault(require("lodash/get"));

var _Utils = require("@Utils");

var _initialState = require("./initialState");

var _services = require("../../services");

var _actionTypes = require("./actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ACTION_CREATOR_NAMERequest = function ACTION_CREATOR_NAMERequest() {
  return {
    type: _actionTypes.ACTION_TYPE_NAME.REQUEST
  };
};

var ACTION_CREATOR_NAMESuccess = function ACTION_CREATOR_NAMESuccess(response) {
  var data = (0, _get2.default)(response, 'data.data', _initialState.initialState.data);
  var code = (0, _get2.default)(response, 'data.code', _initialState.initialState.code);
  return {
    type: _actionTypes.ACTION_TYPE_NAME.SUCCESS,
    data: data,
    code: code
  };
};

var ACTION_CREATOR_NAMEFailure = function ACTION_CREATOR_NAMEFailure(errors) {
  var _handleErrorResponse = (0, _Utils.handleErrorResponse)(errors),
      error = _handleErrorResponse.error,
      code = _handleErrorResponse.code;

  return {
    type: _actionTypes.ACTION_TYPE_NAME.FAILURE,
    error: error,
    code: code
  };
};

var ACTION_CREATOR_NAME = function ACTION_CREATOR_NAME(accountInfo) {
  return function (dispatch) {
    dispatch(ACTION_CREATOR_NAMERequest());
    return (0, _services.SERVICE_NAME)(accountInfo).then(function (response) {
      return dispatch(ACTION_CREATOR_NAMESuccess(response));
    }).catch(function (errors) {
      return dispatch(ACTION_CREATOR_NAMEFailure(errors));
    });
  };
};

exports.ACTION_CREATOR_NAME = ACTION_CREATOR_NAME;