"use strict";

module.exports = "\nimport reducers from './reducers'\nimport { initialState } from './initialState'\nimport { ACTION_CREATOR_NAME } from './actionCreators'\nimport { selectREDUCER_NAME } from './selectors'\n\nexport {\n  reducers,\n  initialState,\n  ACTION_CREATOR_NAME,\n  selectREDUCER_NAME,\n}\n";