"use strict";

module.exports = function (moduleName) {
  return "const ".concat(moduleName, "Routes = (props = { initial: false }) => ({ \n  /** Add some route here */\n})\n\nexport { ").concat(moduleName, "Routes }");
};