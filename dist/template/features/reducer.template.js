"use strict";

module.exports = function (moduleName) {
  return "import { combineReducers } from 'redux'\n\nconst ".concat(moduleName, "Reducers = combineReducers({\n  /* Add some reducer here */\n})\n\nexport { ").concat(moduleName, "Reducers }");
};