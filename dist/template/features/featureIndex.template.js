"use strict";

module.exports = function (moduleName) {
  return "export { ".concat(moduleName, "Routes } from './routes'\nexport { ").concat(moduleName, "Reducers } from './redux/reducers'");
};