"use strict";

module.exports = "\nimport React from 'react'\nimport PropTypes from 'prop-types'\nimport { __%CONTAINER_NAME%__ } from '../containers'\n\nconst __%SCENE_NAME%__ = () => <__%CONTAINER_NAME%__ />\n\n__%SCENE_NAME%__.propTypes = {}\n\nexport default __%SCENE_NAME%__\n";