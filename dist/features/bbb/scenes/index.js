"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CloseAccountScene", {
  enumerable: true,
  get: function get() {
    return _closeAccount.CloseAccountScene;
  }
});

var _closeAccount = require("./closeAccount.scene");