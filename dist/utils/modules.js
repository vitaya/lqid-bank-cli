"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var fs = require('fs');

function isDirSupportIndexExport(path) {
  if (fs.existsSync(path)) {
    var hasExport = fs.readFileSync(path, 'utf8').includes('export');
    return hasExport;
  } else {
    return false;
  }
}

function getModuleExport(path) {
  if (fs.existsSync(path)) {
    var fileContent = fs.readFileSync(path, 'utf8');

    var _fileContent$split = fileContent.split('export'),
        _fileContent$split2 = _slicedToArray(_fileContent$split, 2),
        moduleExport = _fileContent$split2[1];

    return moduleExport.replace(/\n|{|}|\s/g, '').replace('/**Addsomemodulehere*/', '').replace('//!auto-generator-', '').split(',').filter(Boolean);
  } else {
    return [];
  }
}

function getContainerExport(path) {
  var flag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  if (isDirSupportIndexExport("".concat(path, "/index.js"))) {
    return getModuleExport("".concat(path, "/index.js"));
  } else if (flag) {
    var containerList = fs.readdirSync(path);
    return containerList.map(function (conPath) {
      return getContainerExport("".concat(path, "/").concat(conPath), false);
    });
  }
}

function replaceWording(fileContent) {
  var wording = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  // Replace wording in file
  Object.entries(wording).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];

    var pattern = new RegExp(key, 'g');
    fileContent = fileContent.replace(pattern, value);
  });
  return fileContent;
}

function isSupportCommentAutoGenerator(fileContent) {
  return fileContent.includes('auto-generator-import') && fileContent.includes('auto-generator-import');
}

function getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault) {
  if (isImportDefault) {
    return "import ".concat(moduleName, " from '").concat(modulePath.replace('.js', ''), "'");
  }

  return "import { ".concat(moduleName, " } from '").concat(modulePath.replace('.js', ''), "'");
}

function autoImport(moduleName, modulePath, targetPath) {
  var isImportDefault = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var fileContent = fs.readFileSync(targetPath, {
    encoding: 'utf8'
  }); // case has comment auto-generator-import

  if (isSupportCommentAutoGenerator(fileContent)) {
    var _importText = getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault);

    var _importTextReplace = "".concat(_importText, "\n//! auto-generator-import");

    return fileContent.replace('//! auto-generator-import', _importTextReplace);
  }

  var importText = getSelectImportModule(fileContent, moduleName, modulePath, isImportDefault);
  var importTextReplace = "".concat(importText, "\n//! auto-generator-import\n");
  return [importTextReplace].concat(_toConsumableArray(fileContent.split('\n'))).join('\n');
}

function autoExportImportModule(moduleName, modulePath, indexPath) {
  var isImportDefault = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var fileContentIndex = fs.readFileSync(indexPath, {
    encoding: 'utf8'
  });
  var importText = getSelectImportModule(fileContentIndex, moduleName, modulePath, isImportDefault);
  var exportText = "".concat(moduleName);

  if (isSupportCommentAutoGenerator(fileContentIndex)) {
    var importTextReplace = "".concat(importText, "\n//! auto-generator-import");
    var exportTextReplace = "".concat(exportText, ",\n  //! auto-generator-export");
    var finallyFileContent = fileContentIndex.replace('//! auto-generator-import', importTextReplace).replace('//! auto-generator-export', exportTextReplace);
    return finallyFileContent;
  } else {
    var _finallyFileContent = '';

    var _fileContentIndex$spl = fileContentIndex.split('export'),
        _fileContentIndex$spl2 = _slicedToArray(_fileContentIndex$spl, 2),
        importContent = _fileContentIndex$spl2[0],
        exportContent = _fileContentIndex$spl2[1];

    var finallExportContent = ['\n', 'export {'].concat(_toConsumableArray(exportContent.replace(/\n|{|}|\s/g, '').split(',').filter(Boolean).concat([exportText]).map(function (v) {
      return "  ".concat(v, ",");
    })), ['}']).join('\n');
    _finallyFileContent = importContent.split('\n').filter(Boolean).concat([importText], finallExportContent).join('\n');
    return _finallyFileContent;
  }
}

module.exports = {
  isDirSupportIndexExport: isDirSupportIndexExport,
  getModuleExport: getModuleExport,
  getContainerExport: getContainerExport,
  replaceWording: replaceWording,
  autoExportImportModule: autoExportImportModule,
  autoImport: autoImport
};