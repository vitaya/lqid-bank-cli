"use strict";

function searchChoice(choices) {
  return function (answers) {
    var input = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    return new Promise(function (resolve) {
      var choicesFilter = choices.filter(function (v) {
        return v.includes(input);
      });
      resolve(choicesFilter.length === 0 ? [input] : choicesFilter);
    });
  };
}

module.exports = searchChoice;