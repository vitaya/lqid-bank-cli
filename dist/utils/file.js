"use strict";

var fs = require('fs');

var createFile = function createFile(path, data) {
  fs.writeFile(path, data, function (err) {
    if (err) {
      return console.log(err);
    }

    if (typeof callback === 'function') {
      callback();
    }
  });
};

var createDir = function createDir(dir) {
  var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);

    if (typeof callback === 'function') {
      callback();
    }
  }
};

module.exports = {
  createFile: createFile,
  createDir: createDir
};