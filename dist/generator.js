#!/usr/bin/env node
"use strict";

require("@babel/polyfill");

var app = require('commander');

var chalk = require('chalk');

var automateGenerateJob = require('./lib/automate');

var componentGenerator = require('./lib/component');

var featureGenerator = require('./lib/feature');

var sceneGenerator = require('./lib/scene');

var integrateAPI = require('./lib/integrateAPI');

app.version('0.1.2');
app.command('generate <type> <module_name> [path]').alias('gen').description('Generating files follow by argument [automate] ').action(function (type, module_name, path, cmd) {
  if (type === 'automate') {
    console.log(chalk.bold("\n\u2728 Generating ".concat(chalk.green(module_name), "'s automate files \u2728\n")));
    automateGenerateJob(module_name, path);
  } else if (type === 'stl-component') {
    console.log(chalk.bold("\n\u2728 Generating ".concat(chalk.green(module_name), " Stateless Component \u2728\n")));
    componentGenerator(module_name, true, path);
  } else if (type === 'stf-component') {
    console.log(chalk.bold("\n\u2728 Generating ".concat(chalk.green(module_name), " Stateful Component \u2728\n")));
    componentGenerator(module_name, false, path);
  } else if (type === 'feature') {
    console.log(chalk.bold("\n\u2728 Generating ".concat(chalk.green(module_name), " Feature \u2728\n")));
    featureGenerator(module_name, path);
  } else if (type === 'scene') {
    console.log(chalk.bold("\n\u2728 Generating ".concat(chalk.green(module_name), " Scene \u2728\n")));
    sceneGenerator(module_name, path);
  } else {
    console.error('Required some argument');
  }
});
app.command('integrateAPI').alias('intAPI').action(function () {
  integrateAPI();
});
app.parse(process.argv);